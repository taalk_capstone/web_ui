//SearchInfoService.js
'use strict';
app.factory('searchInfoService',  function () {

	 var searchInfoServiceFactory = {};

	 var searchInfo = [];

	 var _setSearchInfo = function(searchInfoData){

	 	searchInfo = searchInfoData;
	 } 

	 var _getSearchInfo = function(){

	 	return searchInfo;
	 }

	 searchInfoServiceFactory.setSearchInfo = _setSearchInfo;
	 searchInfoServiceFactory.getSearchInfo = _getSearchInfo;

	 return searchInfoServiceFactory;

});