$(document).ready(function(){
  $("#navigation li a").on("click", function(e, showInfo){
    e.preventDefault();
    var hrefval = $(this).attr("href");
    
    if(hrefval == "#showInfo") {
        
        if (typeof showInfo == "undefined") {
            //click comes from the >>
            var distance = $('#nav').css('left');
      
          if(distance == "auto" || distance == "0px") {
            $(this).addClass("open");
            openSidepage();
          } else {
            closeSidepage();
          }
        } else {
            //click came from device
            if (showInfo){
                $(this).addClass("open");
                openSidepage();
            } else{
                closeSidepage();
            }
        }
    }
  }); // end click event handler

  function openSidepage() {
    $('#showInfo').show();
    $('#showIcon').text('«');
    $('#nav').animate({
      left: '19%'
    }, 400, 'easeOutBack'); 
  }
  
  function closeSidepage(){
    $('#showInfo').hide();
    $('#showIcon').text('»');
    $("#navigation li a").removeClass("open");
    $('#nav').animate({
      left: '0px'
    }, 400, 'easeOutQuint');  
  }
    
    $("#heatAreasCheckbox").click(function(){
        $("#heatAreas").toggle();
    });
    
     $("#devicesCheckbox").click(function(){
        $("#devices").toggle();
    });
    
     $("#nodesCheckbox").click(function(){
        $("#nodes").toggle();
    });
    
     $("#beaconsCheckbox").click(function(){
        $("#beacons").toggle();
    });
    
    $("#navigationCheckbox").click(function(){
        $("#navigationPath").toggle();
        $(".searchbox").toggle();
    });
    
    //clean the text inside
    $("#search").focus(function() {
        $("#search").val("");
    });
    
    //set the focus on navigation bar
    $("#search").focus();
}); 

function openCloseInfo(showInfo){
    $("#navigation li a").trigger('click', [showInfo]);
    $("#showInfo").scrollTop(0);
}