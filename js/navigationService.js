'use strict';
app.factory('navigationService',  function () {

    var navigationFactory = {};

    navigationFactory.navigate = function (nodes, points, svg) {
        console.info ("navigating to " + points);

        var nSize = points.length;
        var group = svg.append("g")
            .attr("id", "navigationPath");

        for (var i = 0; i < nSize; i++) {

            //pair of points to draw the line
            var p1 = points[i];
            var p2 = points[i+1];

            //the last point has no pair to draw the line
            if (typeof(p2) != "undefined") {
                var line = group.append("line")                
                    .attr("stroke-width", 10)
                    .attr("stroke", "#f1c40f");

                nodes.forEach(function(node){
                    if (node.id == p1){  

                        line.attr("x1", node.center_X)
                            .attr("y1", node.center_Y);

                    }

                    if (node.id == p2){
                        line.attr("x2", node.center_X)
                        .attr("y2", node.center_Y);
                    }

                    });
                }            
        }
    }

    return navigationFactory;

});


