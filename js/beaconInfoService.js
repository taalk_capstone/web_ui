//BeaconInfoService.js
'use strict';
app.factory('beaconInfoService',  function () {

	 var beaconInfoServiceFactory = {};

	  var beaconInfo = {
        beaconId: 0,
        beaconName : '',
        major: 0,
        minor: 0,
        rss: 0,
        tx: 0,
        uuid: null,
        battery: 0,
        Pt_X: 0,
        Pt_Y: 0,
        width: 0,
        height: 0,
        MapId: 0,
        map: null
    };

	 var _setBeaconInfo = function(beaconInfoData){

	 	beaconInfo = beaconInfoData;
	 } 

	 var _getBeaconInfo = function(){

	 	return beaconInfo;
	 }

	 beaconInfoServiceFactory.setBeaconInfo = _setBeaconInfo;
	 beaconInfoServiceFactory.getBeaconInfo = _getBeaconInfo;

	 return beaconInfoServiceFactory;

});