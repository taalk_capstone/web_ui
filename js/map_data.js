var map_data = {
  "commandName": "102",
  "commandResult": "Succeed",
  "resource": {
    "Id": 1,
    "Name": "Conestoga-A wing",
    "MapUnits": [
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 1,
        "unitName": "2A222",
        "lines": [
          {
            "Id": 1,
            "start_X": 138,
            "start_Y": 0,
            "end_X": 174,
            "end_Y": 0,
            "mapUnitId": 1,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 2,
            "start_X": 174,
            "start_Y": 0,
            "end_X": 174,
            "end_Y": 70,
            "mapUnitId": 1,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 3,
            "start_X": 174,
            "start_Y": 70,
            "end_X": 138,
            "end_Y": 70,
            "mapUnitId": 1,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 4,
            "start_X": 138,
            "start_Y": 70,
            "end_X": 138,
            "end_Y": 0,
            "mapUnitId": 1,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 2,
        "unitName": "2A200",
        "lines": [
          {
            "Id": 5,
            "start_X": 138,
            "start_Y": 71,
            "end_X": 174,
            "end_Y": 71,
            "mapUnitId": 2,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 6,
            "start_X": 174,
            "start_Y": 71,
            "end_X": 174,
            "end_Y": 97,
            "mapUnitId": 2,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 7,
            "start_X": 174,
            "start_Y": 97,
            "end_X": 138,
            "end_Y": 97,
            "mapUnitId": 2,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 8,
            "start_X": 138,
            "start_Y": 97,
            "end_X": 138,
            "end_Y": 71,
            "mapUnitId": 2,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 3,
        "unitName": "2A218",
        "lines": [
          {
            "Id": 9,
            "start_X": 138,
            "start_Y": 96,
            "end_X": 181,
            "end_Y": 96,
            "mapUnitId": 3,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 10,
            "start_X": 181,
            "start_Y": 96,
            "end_X": 181,
            "end_Y": 136,
            "mapUnitId": 3,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 11,
            "start_X": 181,
            "start_Y": 136,
            "end_X": 138,
            "end_Y": 136,
            "mapUnitId": 3,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 12,
            "start_X": 138,
            "start_Y": 136,
            "end_X": 138,
            "end_Y": 96,
            "mapUnitId": 3,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 4,
        "unitName": "2A214",
        "lines": [
          {
            "Id": 13,
            "start_X": 138,
            "start_Y": 207,
            "end_X": 180,
            "end_Y": 207,
            "mapUnitId": 4,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 14,
            "start_X": 180,
            "start_Y": 207,
            "end_X": 180,
            "end_Y": 196,
            "mapUnitId": 4,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 15,
            "start_X": 180,
            "start_Y": 196,
            "end_X": 170,
            "end_Y": 189,
            "mapUnitId": 4,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 16,
            "start_X": 170,
            "start_Y": 189,
            "end_X": 156,
            "end_Y": 189,
            "mapUnitId": 4,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 17,
            "start_X": 156,
            "start_Y": 189,
            "end_X": 153,
            "end_Y": 189,
            "mapUnitId": 4,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 18,
            "start_X": 153,
            "start_Y": 189,
            "end_X": 138,
            "end_Y": 189,
            "mapUnitId": 4,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 19,
            "start_X": 138,
            "start_Y": 189,
            "end_X": 138,
            "end_Y": 207,
            "mapUnitId": 4,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 5,
        "unitName": "2A214_2",
        "lines": [
          {
            "Id": 20,
            "start_X": 138,
            "start_Y": 207,
            "end_X": 180,
            "end_Y": 207,
            "mapUnitId": 5,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 21,
            "start_X": 180,
            "start_Y": 207,
            "end_X": 180,
            "end_Y": 220,
            "mapUnitId": 5,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 22,
            "start_X": 180,
            "start_Y": 220,
            "end_X": 138,
            "end_Y": 220,
            "mapUnitId": 5,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 23,
            "start_X": 138,
            "start_Y": 220,
            "end_X": 138,
            "end_Y": 207,
            "mapUnitId": 5,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 6,
        "unitName": "2A212",
        "lines": [
          {
            "Id": 24,
            "start_X": 138,
            "start_Y": 251,
            "end_X": 180,
            "end_Y": 251,
            "mapUnitId": 6,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 25,
            "start_X": 180,
            "start_Y": 251,
            "end_X": 180,
            "end_Y": 220,
            "mapUnitId": 6,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 26,
            "start_X": 180,
            "start_Y": 220,
            "end_X": 138,
            "end_Y": 220,
            "mapUnitId": 6,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 27,
            "start_X": 138,
            "start_Y": 220,
            "end_X": 138,
            "end_Y": 251,
            "mapUnitId": 6,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 7,
        "unitName": "2A210",
        "lines": [
          {
            "Id": 28,
            "start_X": 138,
            "start_Y": 251,
            "end_X": 180,
            "end_Y": 251,
            "mapUnitId": 7,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 29,
            "start_X": 180,
            "start_Y": 251,
            "end_X": 180,
            "end_Y": 281,
            "mapUnitId": 7,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 30,
            "start_X": 180,
            "start_Y": 281,
            "end_X": 138,
            "end_Y": 281,
            "mapUnitId": 7,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 31,
            "start_X": 138,
            "start_Y": 281,
            "end_X": 138,
            "end_Y": 251,
            "mapUnitId": 7,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 8,
        "unitName": "2A208",
        "lines": [
          {
            "Id": 32,
            "start_X": 138,
            "start_Y": 313,
            "end_X": 180,
            "end_Y": 313,
            "mapUnitId": 8,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 33,
            "start_X": 180,
            "start_Y": 313,
            "end_X": 180,
            "end_Y": 281,
            "mapUnitId": 8,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 34,
            "start_X": 180,
            "start_Y": 281,
            "end_X": 138,
            "end_Y": 281,
            "mapUnitId": 8,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 35,
            "start_X": 138,
            "start_Y": 281,
            "end_X": 138,
            "end_Y": 313,
            "mapUnitId": 8,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 9,
        "unitName": "Washroom",
        "lines": [
          {
            "Id": 36,
            "start_X": 138,
            "start_Y": 462,
            "end_X": 151,
            "end_Y": 462,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 37,
            "start_X": 151,
            "start_Y": 462,
            "end_X": 171,
            "end_Y": 462,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 38,
            "start_X": 171,
            "start_Y": 462,
            "end_X": 171,
            "end_Y": 464,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 39,
            "start_X": 171,
            "start_Y": 464,
            "end_X": 194,
            "end_Y": 464,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 40,
            "start_X": 194,
            "start_Y": 464,
            "end_X": 194,
            "end_Y": 462,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 41,
            "start_X": 194,
            "start_Y": 462,
            "end_X": 212,
            "end_Y": 462,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 42,
            "start_X": 212,
            "start_Y": 462,
            "end_X": 212,
            "end_Y": 464,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 43,
            "start_X": 212,
            "start_Y": 464,
            "end_X": 228,
            "end_Y": 464,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 44,
            "start_X": 228,
            "start_Y": 464,
            "end_X": 228,
            "end_Y": 448,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 45,
            "start_X": 228,
            "start_Y": 448,
            "end_X": 228,
            "end_Y": 431,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 46,
            "start_X": 228,
            "start_Y": 431,
            "end_X": 221,
            "end_Y": 431,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 47,
            "start_X": 221,
            "start_Y": 431,
            "end_X": 221,
            "end_Y": 421,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 48,
            "start_X": 221,
            "start_Y": 421,
            "end_X": 204,
            "end_Y": 421,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 49,
            "start_X": 204,
            "start_Y": 421,
            "end_X": 195,
            "end_Y": 421,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 50,
            "start_X": 195,
            "start_Y": 421,
            "end_X": 184,
            "end_Y": 421,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 51,
            "start_X": 184,
            "start_Y": 421,
            "end_X": 168,
            "end_Y": 421,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 52,
            "start_X": 168,
            "start_Y": 421,
            "end_X": 147,
            "end_Y": 421,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 53,
            "start_X": 147,
            "start_Y": 421,
            "end_X": 147,
            "end_Y": 431,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 54,
            "start_X": 147,
            "start_Y": 431,
            "end_X": 138,
            "end_Y": 431,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 55,
            "start_X": 138,
            "start_Y": 431,
            "end_X": 138,
            "end_Y": 448,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 56,
            "start_X": 138,
            "start_Y": 448,
            "end_X": 138,
            "end_Y": 462,
            "mapUnitId": 9,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 10,
        "unitName": "2A202",
        "lines": [
          {
            "Id": 57,
            "start_X": 138,
            "start_Y": 406,
            "end_X": 174,
            "end_Y": 406,
            "mapUnitId": 10,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 58,
            "start_X": 174,
            "start_Y": 406,
            "end_X": 180,
            "end_Y": 401,
            "mapUnitId": 10,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 59,
            "start_X": 180,
            "start_Y": 401,
            "end_X": 180,
            "end_Y": 359,
            "mapUnitId": 10,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 60,
            "start_X": 180,
            "start_Y": 359,
            "end_X": 174,
            "end_Y": 354,
            "mapUnitId": 10,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 61,
            "start_X": 174,
            "start_Y": 354,
            "end_X": 138,
            "end_Y": 354,
            "mapUnitId": 10,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 62,
            "start_X": 138,
            "start_Y": 354,
            "end_X": 138,
            "end_Y": 406,
            "mapUnitId": 10,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 11,
        "unitName": "2A206",
        "lines": [
          {
            "Id": 63,
            "start_X": 138,
            "start_Y": 313,
            "end_X": 180,
            "end_Y": 313,
            "mapUnitId": 11,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 64,
            "start_X": 180,
            "start_Y": 313,
            "end_X": 180,
            "end_Y": 338,
            "mapUnitId": 11,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 65,
            "start_X": 180,
            "start_Y": 338,
            "end_X": 175,
            "end_Y": 344,
            "mapUnitId": 11,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 66,
            "start_X": 175,
            "start_Y": 344,
            "end_X": 138,
            "end_Y": 344,
            "mapUnitId": 11,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 67,
            "start_X": 138,
            "start_Y": 344,
            "end_X": 138,
            "end_Y": 313,
            "mapUnitId": 11,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 12,
        "unitName": "2A216",
        "lines": [
          {
            "Id": 68,
            "start_X": 138,
            "start_Y": 179,
            "end_X": 175,
            "end_Y": 179,
            "mapUnitId": 12,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 69,
            "start_X": 175,
            "start_Y": 179,
            "end_X": 181,
            "end_Y": 172,
            "mapUnitId": 12,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 70,
            "start_X": 181,
            "start_Y": 172,
            "end_X": 181,
            "end_Y": 136,
            "mapUnitId": 12,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 71,
            "start_X": 181,
            "start_Y": 136,
            "end_X": 138,
            "end_Y": 136,
            "mapUnitId": 12,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 72,
            "start_X": 138,
            "start_Y": 136,
            "end_X": 138,
            "end_Y": 179,
            "mapUnitId": 12,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 13,
        "unitName": "Bookstore",
        "lines": [
          {
            "Id": 73,
            "start_X": 231,
            "start_Y": 179,
            "end_X": 195,
            "end_Y": 179,
            "mapUnitId": 13,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 74,
            "start_X": 195,
            "start_Y": 179,
            "end_X": 188,
            "end_Y": 172,
            "mapUnitId": 13,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 75,
            "start_X": 188,
            "start_Y": 172,
            "end_X": 188,
            "end_Y": 97,
            "mapUnitId": 13,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 76,
            "start_X": 188,
            "start_Y": 97,
            "end_X": 231,
            "end_Y": 97,
            "mapUnitId": 13,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 77,
            "start_X": 231,
            "start_Y": 97,
            "end_X": 231,
            "end_Y": 179,
            "mapUnitId": 13,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 14,
        "unitName": "2A224",
        "lines": [
          {
            "Id": 78,
            "start_X": 174,
            "start_Y": 0,
            "end_X": 216,
            "end_Y": 0,
            "mapUnitId": 14,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 79,
            "start_X": 216,
            "start_Y": 0,
            "end_X": 216,
            "end_Y": 27,
            "mapUnitId": 14,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 80,
            "start_X": 216,
            "start_Y": 27,
            "end_X": 174,
            "end_Y": 27,
            "mapUnitId": 14,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 81,
            "start_X": 174,
            "start_Y": 27,
            "end_X": 174,
            "end_Y": 0,
            "mapUnitId": 14,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 15,
        "unitName": "",
        "lines": [
          {
            "Id": 82,
            "start_X": 219,
            "start_Y": 75,
            "end_X": 231,
            "end_Y": 75,
            "mapUnitId": 15,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 83,
            "start_X": 231,
            "start_Y": 75,
            "end_X": 231,
            "end_Y": 97,
            "mapUnitId": 15,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 84,
            "start_X": 231,
            "start_Y": 97,
            "end_X": 219,
            "end_Y": 97,
            "mapUnitId": 15,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 85,
            "start_X": 219,
            "start_Y": 97,
            "end_X": 219,
            "end_Y": 75,
            "mapUnitId": 15,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 16,
        "unitName": "2A316",
        "lines": [
          {
            "Id": 86,
            "start_X": 266,
            "start_Y": 81,
            "end_X": 266,
            "end_Y": 180,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 87,
            "start_X": 266,
            "start_Y": 180,
            "end_X": 250,
            "end_Y": 180,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 88,
            "start_X": 250,
            "start_Y": 180,
            "end_X": 231,
            "end_Y": 180,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 89,
            "start_X": 231,
            "start_Y": 180,
            "end_X": 231,
            "end_Y": 154,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 90,
            "start_X": 231,
            "start_Y": 154,
            "end_X": 231,
            "end_Y": 143,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 91,
            "start_X": 231,
            "start_Y": 143,
            "end_X": 231,
            "end_Y": 96,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 92,
            "start_X": 231,
            "start_Y": 96,
            "end_X": 231,
            "end_Y": 81,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 93,
            "start_X": 231,
            "start_Y": 81,
            "end_X": 231,
            "end_Y": 66,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 94,
            "start_X": 231,
            "start_Y": 66,
            "end_X": 231,
            "end_Y": 50,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 95,
            "start_X": 231,
            "start_Y": 50,
            "end_X": 266,
            "end_Y": 50,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 96,
            "start_X": 266,
            "start_Y": 50,
            "end_X": 266,
            "end_Y": 66,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 97,
            "start_X": 266,
            "start_Y": 66,
            "end_X": 266,
            "end_Y": 81,
            "mapUnitId": 16,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 17,
        "unitName": "2A322",
        "lines": [
          {
            "Id": 98,
            "start_X": 327,
            "start_Y": 82,
            "end_X": 298,
            "end_Y": 82,
            "mapUnitId": 17,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 99,
            "start_X": 298,
            "start_Y": 82,
            "end_X": 298,
            "end_Y": 110,
            "mapUnitId": 17,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 100,
            "start_X": 298,
            "start_Y": 110,
            "end_X": 298,
            "end_Y": 126,
            "mapUnitId": 17,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 101,
            "start_X": 298,
            "start_Y": 126,
            "end_X": 327,
            "end_Y": 126,
            "mapUnitId": 17,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 102,
            "start_X": 327,
            "start_Y": 126,
            "end_X": 327,
            "end_Y": 110,
            "mapUnitId": 17,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 103,
            "start_X": 327,
            "start_Y": 110,
            "end_X": 327,
            "end_Y": 82,
            "mapUnitId": 17,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 18,
        "unitName": "2A511",
        "lines": [
          {
            "Id": 104,
            "start_X": 459,
            "start_Y": 84,
            "end_X": 327,
            "end_Y": 84,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 105,
            "start_X": 327,
            "start_Y": 84,
            "end_X": 327,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 106,
            "start_X": 327,
            "start_Y": 156,
            "end_X": 383,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 107,
            "start_X": 383,
            "start_Y": 156,
            "end_X": 370,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 108,
            "start_X": 370,
            "start_Y": 156,
            "end_X": 354,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 109,
            "start_X": 354,
            "start_Y": 156,
            "end_X": 339,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 110,
            "start_X": 339,
            "start_Y": 156,
            "end_X": 327,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 111,
            "start_X": 327,
            "start_Y": 156,
            "end_X": 327,
            "end_Y": 168,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 112,
            "start_X": 327,
            "start_Y": 168,
            "end_X": 327,
            "end_Y": 180,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 113,
            "start_X": 327,
            "start_Y": 180,
            "end_X": 339,
            "end_Y": 180,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 114,
            "start_X": 339,
            "start_Y": 180,
            "end_X": 339,
            "end_Y": 168,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 115,
            "start_X": 339,
            "start_Y": 168,
            "end_X": 354,
            "end_Y": 168,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 116,
            "start_X": 354,
            "start_Y": 168,
            "end_X": 358,
            "end_Y": 168,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 117,
            "start_X": 358,
            "start_Y": 168,
            "end_X": 358,
            "end_Y": 180,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 118,
            "start_X": 358,
            "start_Y": 180,
            "end_X": 376,
            "end_Y": 180,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 119,
            "start_X": 376,
            "start_Y": 180,
            "end_X": 376,
            "end_Y": 177,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 120,
            "start_X": 376,
            "start_Y": 177,
            "end_X": 383,
            "end_Y": 177,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 121,
            "start_X": 383,
            "start_Y": 177,
            "end_X": 386,
            "end_Y": 182,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 122,
            "start_X": 386,
            "start_Y": 182,
            "end_X": 399,
            "end_Y": 182,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 123,
            "start_X": 399,
            "start_Y": 182,
            "end_X": 422,
            "end_Y": 182,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 124,
            "start_X": 422,
            "start_Y": 182,
            "end_X": 459,
            "end_Y": 182,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 125,
            "start_X": 459,
            "start_Y": 182,
            "end_X": 459,
            "end_Y": 168,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 126,
            "start_X": 459,
            "start_Y": 168,
            "end_X": 459,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 127,
            "start_X": 459,
            "start_Y": 156,
            "end_X": 422,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 128,
            "start_X": 422,
            "start_Y": 156,
            "end_X": 399,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 129,
            "start_X": 399,
            "start_Y": 156,
            "end_X": 459,
            "end_Y": 156,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 130,
            "start_X": 459,
            "start_Y": 156,
            "end_X": 459,
            "end_Y": 84,
            "mapUnitId": 18,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 19,
        "unitName": "2A623",
        "lines": [
          {
            "Id": 131,
            "start_X": 459,
            "start_Y": 84,
            "end_X": 508,
            "end_Y": 84,
            "mapUnitId": 19,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 132,
            "start_X": 508,
            "start_Y": 84,
            "end_X": 508,
            "end_Y": 138,
            "mapUnitId": 19,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 133,
            "start_X": 508,
            "start_Y": 138,
            "end_X": 508,
            "end_Y": 156,
            "mapUnitId": 19,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 134,
            "start_X": 508,
            "start_Y": 156,
            "end_X": 508,
            "end_Y": 177,
            "mapUnitId": 19,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 135,
            "start_X": 508,
            "start_Y": 177,
            "end_X": 481,
            "end_Y": 177,
            "mapUnitId": 19,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 136,
            "start_X": 481,
            "start_Y": 177,
            "end_X": 459,
            "end_Y": 177,
            "mapUnitId": 19,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 137,
            "start_X": 459,
            "start_Y": 177,
            "end_X": 459,
            "end_Y": 156,
            "mapUnitId": 19,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 138,
            "start_X": 459,
            "start_Y": 156,
            "end_X": 459,
            "end_Y": 84,
            "mapUnitId": 19,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 20,
        "unitName": "2A315",
        "lines": [
          {
            "Id": 139,
            "start_X": 327,
            "start_Y": 126,
            "end_X": 298,
            "end_Y": 126,
            "mapUnitId": 20,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 140,
            "start_X": 298,
            "start_Y": 126,
            "end_X": 298,
            "end_Y": 132,
            "mapUnitId": 20,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 141,
            "start_X": 298,
            "start_Y": 132,
            "end_X": 298,
            "end_Y": 156,
            "mapUnitId": 20,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 142,
            "start_X": 298,
            "start_Y": 156,
            "end_X": 298,
            "end_Y": 180,
            "mapUnitId": 20,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 143,
            "start_X": 298,
            "start_Y": 180,
            "end_X": 327,
            "end_Y": 180,
            "mapUnitId": 20,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 144,
            "start_X": 327,
            "start_Y": 180,
            "end_X": 327,
            "end_Y": 156,
            "mapUnitId": 20,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 145,
            "start_X": 327,
            "start_Y": 156,
            "end_X": 327,
            "end_Y": 132,
            "mapUnitId": 20,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 146,
            "start_X": 327,
            "start_Y": 132,
            "end_X": 327,
            "end_Y": 126,
            "mapUnitId": 20,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 21,
        "unitName": "",
        "lines": [
          {
            "Id": 147,
            "start_X": 293,
            "start_Y": 82,
            "end_X": 293,
            "end_Y": 93,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 148,
            "start_X": 293,
            "start_Y": 93,
            "end_X": 293,
            "end_Y": 125,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 149,
            "start_X": 293,
            "start_Y": 125,
            "end_X": 293,
            "end_Y": 154,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 150,
            "start_X": 293,
            "start_Y": 154,
            "end_X": 293,
            "end_Y": 180,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 151,
            "start_X": 293,
            "start_Y": 180,
            "end_X": 264,
            "end_Y": 180,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 152,
            "start_X": 264,
            "start_Y": 180,
            "end_X": 264,
            "end_Y": 156,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 153,
            "start_X": 264,
            "start_Y": 156,
            "end_X": 260,
            "end_Y": 156,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 154,
            "start_X": 260,
            "start_Y": 156,
            "end_X": 260,
            "end_Y": 154,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 155,
            "start_X": 260,
            "start_Y": 154,
            "end_X": 260,
            "end_Y": 125,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 156,
            "start_X": 260,
            "start_Y": 125,
            "end_X": 266,
            "end_Y": 125,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 157,
            "start_X": 266,
            "start_Y": 125,
            "end_X": 266,
            "end_Y": 82,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 158,
            "start_X": 266,
            "start_Y": 82,
            "end_X": 266,
            "end_Y": 50,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 159,
            "start_X": 266,
            "start_Y": 50,
            "end_X": 293,
            "end_Y": 50,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 160,
            "start_X": 293,
            "start_Y": 50,
            "end_X": 293,
            "end_Y": 82,
            "mapUnitId": 21,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 22,
        "unitName": "2A314",
        "lines": [
          {
            "Id": 161,
            "start_X": 232,
            "start_Y": 190,
            "end_X": 272,
            "end_Y": 190,
            "mapUnitId": 22,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 162,
            "start_X": 272,
            "start_Y": 190,
            "end_X": 272,
            "end_Y": 223,
            "mapUnitId": 22,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 163,
            "start_X": 272,
            "start_Y": 223,
            "end_X": 232,
            "end_Y": 223,
            "mapUnitId": 22,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 164,
            "start_X": 232,
            "start_Y": 223,
            "end_X": 232,
            "end_Y": 190,
            "mapUnitId": 22,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 23,
        "unitName": "2A312",
        "lines": [
          {
            "Id": 165,
            "start_X": 232,
            "start_Y": 222,
            "end_X": 272,
            "end_Y": 222,
            "mapUnitId": 23,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 166,
            "start_X": 272,
            "start_Y": 222,
            "end_X": 272,
            "end_Y": 252,
            "mapUnitId": 23,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 167,
            "start_X": 272,
            "start_Y": 252,
            "end_X": 232,
            "end_Y": 252,
            "mapUnitId": 23,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 168,
            "start_X": 232,
            "start_Y": 252,
            "end_X": 232,
            "end_Y": 222,
            "mapUnitId": 23,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 24,
        "unitName": "2A313",
        "lines": [
          {
            "Id": 169,
            "start_X": 284,
            "start_Y": 191,
            "end_X": 327,
            "end_Y": 191,
            "mapUnitId": 24,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 170,
            "start_X": 327,
            "start_Y": 191,
            "end_X": 327,
            "end_Y": 206,
            "mapUnitId": 24,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 171,
            "start_X": 327,
            "start_Y": 206,
            "end_X": 284,
            "end_Y": 206,
            "mapUnitId": 24,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 172,
            "start_X": 284,
            "start_Y": 206,
            "end_X": 284,
            "end_Y": 191,
            "mapUnitId": 24,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 25,
        "unitName": "2A307",
        "lines": [
          {
            "Id": 173,
            "start_X": 284,
            "start_Y": 205,
            "end_X": 327,
            "end_Y": 205,
            "mapUnitId": 25,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 174,
            "start_X": 327,
            "start_Y": 205,
            "end_X": 327,
            "end_Y": 283,
            "mapUnitId": 25,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 175,
            "start_X": 327,
            "start_Y": 283,
            "end_X": 284,
            "end_Y": 283,
            "mapUnitId": 25,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 176,
            "start_X": 284,
            "start_Y": 283,
            "end_X": 284,
            "end_Y": 205,
            "mapUnitId": 25,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 26,
        "unitName": "2A305",
        "lines": [
          {
            "Id": 177,
            "start_X": 284,
            "start_Y": 283,
            "end_X": 327,
            "end_Y": 283,
            "mapUnitId": 26,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 178,
            "start_X": 327,
            "start_Y": 283,
            "end_X": 327,
            "end_Y": 313,
            "mapUnitId": 26,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 179,
            "start_X": 327,
            "start_Y": 313,
            "end_X": 284,
            "end_Y": 313,
            "mapUnitId": 26,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 180,
            "start_X": 284,
            "start_Y": 313,
            "end_X": 284,
            "end_Y": 283,
            "mapUnitId": 26,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 27,
        "unitName": "2A303",
        "lines": [
          {
            "Id": 181,
            "start_X": 284,
            "start_Y": 313,
            "end_X": 317,
            "end_Y": 313,
            "mapUnitId": 27,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 182,
            "start_X": 317,
            "start_Y": 313,
            "end_X": 327,
            "end_Y": 313,
            "mapUnitId": 27,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 183,
            "start_X": 327,
            "start_Y": 313,
            "end_X": 327,
            "end_Y": 345,
            "mapUnitId": 27,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 184,
            "start_X": 327,
            "start_Y": 345,
            "end_X": 317,
            "end_Y": 345,
            "mapUnitId": 27,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 185,
            "start_X": 317,
            "start_Y": 345,
            "end_X": 284,
            "end_Y": 345,
            "mapUnitId": 27,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 186,
            "start_X": 284,
            "start_Y": 345,
            "end_X": 284,
            "end_Y": 313,
            "mapUnitId": 27,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 28,
        "unitName": "2A310",
        "lines": [
          {
            "Id": 187,
            "start_X": 232,
            "start_Y": 252,
            "end_X": 272,
            "end_Y": 252,
            "mapUnitId": 28,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 188,
            "start_X": 272,
            "start_Y": 252,
            "end_X": 272,
            "end_Y": 282,
            "mapUnitId": 28,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 189,
            "start_X": 272,
            "start_Y": 282,
            "end_X": 232,
            "end_Y": 282,
            "mapUnitId": 28,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 190,
            "start_X": 232,
            "start_Y": 282,
            "end_X": 232,
            "end_Y": 252,
            "mapUnitId": 28,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 29,
        "unitName": "2A308",
        "lines": [
          {
            "Id": 191,
            "start_X": 232,
            "start_Y": 282,
            "end_X": 272,
            "end_Y": 282,
            "mapUnitId": 29,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 192,
            "start_X": 272,
            "start_Y": 282,
            "end_X": 272,
            "end_Y": 312,
            "mapUnitId": 29,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 193,
            "start_X": 272,
            "start_Y": 312,
            "end_X": 232,
            "end_Y": 312,
            "mapUnitId": 29,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 194,
            "start_X": 232,
            "start_Y": 312,
            "end_X": 232,
            "end_Y": 282,
            "mapUnitId": 29,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 30,
        "unitName": "2A306",
        "lines": [
          {
            "Id": 195,
            "start_X": 232,
            "start_Y": 311,
            "end_X": 272,
            "end_Y": 311,
            "mapUnitId": 30,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 196,
            "start_X": 272,
            "start_Y": 311,
            "end_X": 272,
            "end_Y": 345,
            "mapUnitId": 30,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 197,
            "start_X": 272,
            "start_Y": 345,
            "end_X": 232,
            "end_Y": 345,
            "mapUnitId": 30,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 198,
            "start_X": 232,
            "start_Y": 345,
            "end_X": 232,
            "end_Y": 311,
            "mapUnitId": 30,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 31,
        "unitName": "2A213",
        "lines": [
          {
            "Id": 199,
            "start_X": 191,
            "start_Y": 190,
            "end_X": 231,
            "end_Y": 190,
            "mapUnitId": 31,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 200,
            "start_X": 231,
            "start_Y": 190,
            "end_X": 231,
            "end_Y": 221,
            "mapUnitId": 31,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 201,
            "start_X": 231,
            "start_Y": 221,
            "end_X": 191,
            "end_Y": 221,
            "mapUnitId": 31,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 202,
            "start_X": 191,
            "start_Y": 221,
            "end_X": 191,
            "end_Y": 190,
            "mapUnitId": 31,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 32,
        "unitName": "2A211",
        "lines": [
          {
            "Id": 203,
            "start_X": 191,
            "start_Y": 220,
            "end_X": 232,
            "end_Y": 220,
            "mapUnitId": 32,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 204,
            "start_X": 232,
            "start_Y": 220,
            "end_X": 232,
            "end_Y": 256,
            "mapUnitId": 32,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 205,
            "start_X": 232,
            "start_Y": 256,
            "end_X": 191,
            "end_Y": 256,
            "mapUnitId": 32,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 206,
            "start_X": 191,
            "start_Y": 256,
            "end_X": 191,
            "end_Y": 220,
            "mapUnitId": 32,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 33,
        "unitName": "2A209",
        "lines": [
          {
            "Id": 207,
            "start_X": 191,
            "start_Y": 256,
            "end_X": 232,
            "end_Y": 256,
            "mapUnitId": 33,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 208,
            "start_X": 232,
            "start_Y": 256,
            "end_X": 232,
            "end_Y": 282,
            "mapUnitId": 33,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 209,
            "start_X": 232,
            "start_Y": 282,
            "end_X": 191,
            "end_Y": 282,
            "mapUnitId": 33,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 210,
            "start_X": 191,
            "start_Y": 282,
            "end_X": 191,
            "end_Y": 256,
            "mapUnitId": 33,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 34,
        "unitName": "2A207",
        "lines": [
          {
            "Id": 211,
            "start_X": 191,
            "start_Y": 282,
            "end_X": 232,
            "end_Y": 282,
            "mapUnitId": 34,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 212,
            "start_X": 232,
            "start_Y": 282,
            "end_X": 232,
            "end_Y": 311,
            "mapUnitId": 34,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 213,
            "start_X": 232,
            "start_Y": 311,
            "end_X": 191,
            "end_Y": 311,
            "mapUnitId": 34,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 214,
            "start_X": 191,
            "start_Y": 311,
            "end_X": 191,
            "end_Y": 282,
            "mapUnitId": 34,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 35,
        "unitName": "2A203",
        "lines": [
          {
            "Id": 215,
            "start_X": 232,
            "start_Y": 380,
            "end_X": 191,
            "end_Y": 380,
            "mapUnitId": 35,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 216,
            "start_X": 191,
            "start_Y": 380,
            "end_X": 191,
            "end_Y": 360,
            "mapUnitId": 35,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 217,
            "start_X": 191,
            "start_Y": 360,
            "end_X": 196,
            "end_Y": 355,
            "mapUnitId": 35,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 218,
            "start_X": 196,
            "start_Y": 355,
            "end_X": 232,
            "end_Y": 355,
            "mapUnitId": 35,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 219,
            "start_X": 232,
            "start_Y": 355,
            "end_X": 232,
            "end_Y": 380,
            "mapUnitId": 35,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 36,
        "unitName": "2A201",
        "lines": [
          {
            "Id": 220,
            "start_X": 232,
            "start_Y": 380,
            "end_X": 191,
            "end_Y": 380,
            "mapUnitId": 36,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 221,
            "start_X": 191,
            "start_Y": 380,
            "end_X": 191,
            "end_Y": 401,
            "mapUnitId": 36,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 222,
            "start_X": 191,
            "start_Y": 401,
            "end_X": 196,
            "end_Y": 406,
            "mapUnitId": 36,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 223,
            "start_X": 196,
            "start_Y": 406,
            "end_X": 232,
            "end_Y": 406,
            "mapUnitId": 36,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 224,
            "start_X": 232,
            "start_Y": 406,
            "end_X": 232,
            "end_Y": 380,
            "mapUnitId": 36,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 37,
        "unitName": "2A304",
        "lines": [
          {
            "Id": 225,
            "start_X": 232,
            "start_Y": 380,
            "end_X": 273,
            "end_Y": 380,
            "mapUnitId": 37,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 226,
            "start_X": 273,
            "start_Y": 380,
            "end_X": 273,
            "end_Y": 360,
            "mapUnitId": 37,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 227,
            "start_X": 273,
            "start_Y": 360,
            "end_X": 268,
            "end_Y": 355,
            "mapUnitId": 37,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 228,
            "start_X": 268,
            "start_Y": 355,
            "end_X": 232,
            "end_Y": 355,
            "mapUnitId": 37,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 229,
            "start_X": 232,
            "start_Y": 355,
            "end_X": 232,
            "end_Y": 380,
            "mapUnitId": 37,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 38,
        "unitName": "2A301",
        "lines": [
          {
            "Id": 230,
            "start_X": 283,
            "start_Y": 381,
            "end_X": 283,
            "end_Y": 360,
            "mapUnitId": 38,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 231,
            "start_X": 283,
            "start_Y": 360,
            "end_X": 289,
            "end_Y": 355,
            "mapUnitId": 38,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 232,
            "start_X": 289,
            "start_Y": 355,
            "end_X": 326,
            "end_Y": 355,
            "mapUnitId": 38,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 233,
            "start_X": 326,
            "start_Y": 355,
            "end_X": 326,
            "end_Y": 381,
            "mapUnitId": 38,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 234,
            "start_X": 326,
            "start_Y": 381,
            "end_X": 326,
            "end_Y": 407,
            "mapUnitId": 38,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 235,
            "start_X": 326,
            "start_Y": 407,
            "end_X": 289,
            "end_Y": 407,
            "mapUnitId": 38,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 236,
            "start_X": 289,
            "start_Y": 407,
            "end_X": 283,
            "end_Y": 402,
            "mapUnitId": 38,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 237,
            "start_X": 283,
            "start_Y": 402,
            "end_X": 283,
            "end_Y": 381,
            "mapUnitId": 38,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 39,
        "unitName": "2A140",
        "lines": [
          {
            "Id": 238,
            "start_X": 365,
            "start_Y": 355,
            "end_X": 326,
            "end_Y": 355,
            "mapUnitId": 39,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 239,
            "start_X": 326,
            "start_Y": 355,
            "end_X": 326,
            "end_Y": 407,
            "mapUnitId": 39,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 240,
            "start_X": 326,
            "start_Y": 407,
            "end_X": 365,
            "end_Y": 407,
            "mapUnitId": 39,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 241,
            "start_X": 365,
            "start_Y": 407,
            "end_X": 365,
            "end_Y": 355,
            "mapUnitId": 39,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 40,
        "unitName": "2A502",
        "lines": [
          {
            "Id": 242,
            "start_X": 408,
            "start_Y": 381,
            "end_X": 408,
            "end_Y": 360,
            "mapUnitId": 40,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 243,
            "start_X": 408,
            "start_Y": 360,
            "end_X": 403,
            "end_Y": 355,
            "mapUnitId": 40,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 244,
            "start_X": 403,
            "start_Y": 355,
            "end_X": 366,
            "end_Y": 355,
            "mapUnitId": 40,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 245,
            "start_X": 366,
            "start_Y": 355,
            "end_X": 366,
            "end_Y": 381,
            "mapUnitId": 40,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 246,
            "start_X": 366,
            "start_Y": 381,
            "end_X": 366,
            "end_Y": 407,
            "mapUnitId": 40,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 247,
            "start_X": 366,
            "start_Y": 407,
            "end_X": 403,
            "end_Y": 407,
            "mapUnitId": 40,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 248,
            "start_X": 403,
            "start_Y": 407,
            "end_X": 408,
            "end_Y": 402,
            "mapUnitId": 40,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 249,
            "start_X": 408,
            "start_Y": 402,
            "end_X": 408,
            "end_Y": 381,
            "mapUnitId": 40,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 41,
        "unitName": "2A302",
        "lines": [
          {
            "Id": 250,
            "start_X": 232,
            "start_Y": 380,
            "end_X": 273,
            "end_Y": 380,
            "mapUnitId": 41,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 251,
            "start_X": 273,
            "start_Y": 380,
            "end_X": 273,
            "end_Y": 401,
            "mapUnitId": 41,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 252,
            "start_X": 273,
            "start_Y": 401,
            "end_X": 268,
            "end_Y": 406,
            "mapUnitId": 41,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 253,
            "start_X": 268,
            "start_Y": 406,
            "end_X": 232,
            "end_Y": 406,
            "mapUnitId": 41,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 254,
            "start_X": 232,
            "start_Y": 406,
            "end_X": 232,
            "end_Y": 380,
            "mapUnitId": 41,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 42,
        "unitName": "2A205",
        "lines": [
          {
            "Id": 255,
            "start_X": 232,
            "start_Y": 311,
            "end_X": 191,
            "end_Y": 311,
            "mapUnitId": 42,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 256,
            "start_X": 191,
            "start_Y": 311,
            "end_X": 191,
            "end_Y": 339,
            "mapUnitId": 42,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 257,
            "start_X": 191,
            "start_Y": 339,
            "end_X": 196,
            "end_Y": 345,
            "mapUnitId": 42,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 258,
            "start_X": 196,
            "start_Y": 345,
            "end_X": 232,
            "end_Y": 345,
            "mapUnitId": 42,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 259,
            "start_X": 232,
            "start_Y": 345,
            "end_X": 232,
            "end_Y": 311,
            "mapUnitId": 42,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 43,
        "unitName": "2A413",
        "lines": [
          {
            "Id": 260,
            "start_X": 338,
            "start_Y": 191,
            "end_X": 377,
            "end_Y": 191,
            "mapUnitId": 43,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 261,
            "start_X": 377,
            "start_Y": 191,
            "end_X": 377,
            "end_Y": 214,
            "mapUnitId": 43,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 262,
            "start_X": 377,
            "start_Y": 214,
            "end_X": 338,
            "end_Y": 214,
            "mapUnitId": 43,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 263,
            "start_X": 338,
            "start_Y": 214,
            "end_X": 338,
            "end_Y": 191,
            "mapUnitId": 43,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 44,
        "unitName": "2A411",
        "lines": [
          {
            "Id": 264,
            "start_X": 338,
            "start_Y": 214,
            "end_X": 377,
            "end_Y": 214,
            "mapUnitId": 44,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 265,
            "start_X": 377,
            "start_Y": 214,
            "end_X": 377,
            "end_Y": 237,
            "mapUnitId": 44,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 266,
            "start_X": 377,
            "start_Y": 237,
            "end_X": 338,
            "end_Y": 237,
            "mapUnitId": 44,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 267,
            "start_X": 338,
            "start_Y": 237,
            "end_X": 338,
            "end_Y": 214,
            "mapUnitId": 44,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 45,
        "unitName": "2A409",
        "lines": [
          {
            "Id": 268,
            "start_X": 338,
            "start_Y": 235,
            "end_X": 377,
            "end_Y": 235,
            "mapUnitId": 45,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 269,
            "start_X": 377,
            "start_Y": 235,
            "end_X": 377,
            "end_Y": 256,
            "mapUnitId": 45,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 270,
            "start_X": 377,
            "start_Y": 256,
            "end_X": 338,
            "end_Y": 256,
            "mapUnitId": 45,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 271,
            "start_X": 338,
            "start_Y": 256,
            "end_X": 338,
            "end_Y": 235,
            "mapUnitId": 45,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 46,
        "unitName": "2A407",
        "lines": [
          {
            "Id": 272,
            "start_X": 338,
            "start_Y": 258,
            "end_X": 377,
            "end_Y": 258,
            "mapUnitId": 46,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 273,
            "start_X": 377,
            "start_Y": 258,
            "end_X": 377,
            "end_Y": 281,
            "mapUnitId": 46,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 274,
            "start_X": 377,
            "start_Y": 281,
            "end_X": 338,
            "end_Y": 281,
            "mapUnitId": 46,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 275,
            "start_X": 338,
            "start_Y": 281,
            "end_X": 338,
            "end_Y": 258,
            "mapUnitId": 46,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 47,
        "unitName": "2A405",
        "lines": [
          {
            "Id": 276,
            "start_X": 338,
            "start_Y": 280,
            "end_X": 377,
            "end_Y": 280,
            "mapUnitId": 47,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 277,
            "start_X": 377,
            "start_Y": 280,
            "end_X": 377,
            "end_Y": 302,
            "mapUnitId": 47,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 278,
            "start_X": 377,
            "start_Y": 302,
            "end_X": 338,
            "end_Y": 302,
            "mapUnitId": 47,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 279,
            "start_X": 338,
            "start_Y": 302,
            "end_X": 338,
            "end_Y": 280,
            "mapUnitId": 47,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 48,
        "unitName": "2A403",
        "lines": [
          {
            "Id": 280,
            "start_X": 338,
            "start_Y": 314,
            "end_X": 370,
            "end_Y": 314,
            "mapUnitId": 48,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 281,
            "start_X": 370,
            "start_Y": 314,
            "end_X": 370,
            "end_Y": 346,
            "mapUnitId": 48,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 282,
            "start_X": 370,
            "start_Y": 346,
            "end_X": 338,
            "end_Y": 346,
            "mapUnitId": 48,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 283,
            "start_X": 338,
            "start_Y": 346,
            "end_X": 338,
            "end_Y": 314,
            "mapUnitId": 48,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 49,
        "unitName": "2A503",
        "lines": [
          {
            "Id": 284,
            "start_X": 461,
            "start_Y": 346,
            "end_X": 419,
            "end_Y": 346,
            "mapUnitId": 49,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 285,
            "start_X": 419,
            "start_Y": 346,
            "end_X": 419,
            "end_Y": 314,
            "mapUnitId": 49,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 286,
            "start_X": 419,
            "start_Y": 314,
            "end_X": 461,
            "end_Y": 314,
            "mapUnitId": 49,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 287,
            "start_X": 461,
            "start_Y": 314,
            "end_X": 482,
            "end_Y": 314,
            "mapUnitId": 49,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 288,
            "start_X": 482,
            "start_Y": 314,
            "end_X": 482,
            "end_Y": 334,
            "mapUnitId": 49,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 289,
            "start_X": 482,
            "start_Y": 334,
            "end_X": 482,
            "end_Y": 354,
            "mapUnitId": 49,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 290,
            "start_X": 482,
            "start_Y": 354,
            "end_X": 461,
            "end_Y": 354,
            "mapUnitId": 49,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 291,
            "start_X": 461,
            "start_Y": 354,
            "end_X": 461,
            "end_Y": 346,
            "mapUnitId": 49,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 50,
        "unitName": "2A609",
        "lines": [
          {
            "Id": 292,
            "start_X": 488,
            "start_Y": 334,
            "end_X": 488,
            "end_Y": 314,
            "mapUnitId": 50,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 293,
            "start_X": 488,
            "start_Y": 314,
            "end_X": 508,
            "end_Y": 314,
            "mapUnitId": 50,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 294,
            "start_X": 508,
            "start_Y": 314,
            "end_X": 508,
            "end_Y": 334,
            "mapUnitId": 50,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 295,
            "start_X": 508,
            "start_Y": 334,
            "end_X": 508,
            "end_Y": 346,
            "mapUnitId": 50,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 296,
            "start_X": 508,
            "start_Y": 346,
            "end_X": 508,
            "end_Y": 377,
            "mapUnitId": 50,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 297,
            "start_X": 508,
            "start_Y": 377,
            "end_X": 488,
            "end_Y": 377,
            "mapUnitId": 50,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 298,
            "start_X": 488,
            "start_Y": 377,
            "end_X": 488,
            "end_Y": 346,
            "mapUnitId": 50,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 299,
            "start_X": 488,
            "start_Y": 346,
            "end_X": 488,
            "end_Y": 334,
            "mapUnitId": 50,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 51,
        "unitName": "2A605",
        "lines": [
          {
            "Id": 300,
            "start_X": 508,
            "start_Y": 377,
            "end_X": 488,
            "end_Y": 377,
            "mapUnitId": 51,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 301,
            "start_X": 488,
            "start_Y": 377,
            "end_X": 488,
            "end_Y": 408,
            "mapUnitId": 51,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 302,
            "start_X": 488,
            "start_Y": 408,
            "end_X": 488,
            "end_Y": 429,
            "mapUnitId": 51,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 303,
            "start_X": 488,
            "start_Y": 429,
            "end_X": 508,
            "end_Y": 429,
            "mapUnitId": 51,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 304,
            "start_X": 508,
            "start_Y": 429,
            "end_X": 508,
            "end_Y": 408,
            "mapUnitId": 51,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 305,
            "start_X": 508,
            "start_Y": 408,
            "end_X": 508,
            "end_Y": 377,
            "mapUnitId": 51,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 52,
        "unitName": "2A601",
        "lines": [
          {
            "Id": 306,
            "start_X": 473,
            "start_Y": 429,
            "end_X": 513,
            "end_Y": 429,
            "mapUnitId": 52,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 307,
            "start_X": 513,
            "start_Y": 429,
            "end_X": 513,
            "end_Y": 454,
            "mapUnitId": 52,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 308,
            "start_X": 513,
            "start_Y": 454,
            "end_X": 473,
            "end_Y": 454,
            "mapUnitId": 52,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 309,
            "start_X": 473,
            "start_Y": 454,
            "end_X": 473,
            "end_Y": 429,
            "mapUnitId": 52,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 53,
        "unitName": "2A147-1",
        "lines": [
          {
            "Id": 310,
            "start_X": 449,
            "start_Y": 418,
            "end_X": 461,
            "end_Y": 418,
            "mapUnitId": 53,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 311,
            "start_X": 461,
            "start_Y": 418,
            "end_X": 461,
            "end_Y": 449,
            "mapUnitId": 53,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 312,
            "start_X": 461,
            "start_Y": 449,
            "end_X": 449,
            "end_Y": 449,
            "mapUnitId": 53,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 313,
            "start_X": 449,
            "start_Y": 449,
            "end_X": 449,
            "end_Y": 418,
            "mapUnitId": 53,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 54,
        "unitName": "2A147",
        "lines": [
          {
            "Id": 314,
            "start_X": 420,
            "start_Y": 418,
            "end_X": 449,
            "end_Y": 418,
            "mapUnitId": 54,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 315,
            "start_X": 449,
            "start_Y": 418,
            "end_X": 449,
            "end_Y": 449,
            "mapUnitId": 54,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 316,
            "start_X": 449,
            "start_Y": 449,
            "end_X": 420,
            "end_Y": 449,
            "mapUnitId": 54,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 317,
            "start_X": 420,
            "start_Y": 449,
            "end_X": 420,
            "end_Y": 418,
            "mapUnitId": 54,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 55,
        "unitName": "2A145",
        "lines": [
          {
            "Id": 318,
            "start_X": 388,
            "start_Y": 418,
            "end_X": 420,
            "end_Y": 418,
            "mapUnitId": 55,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 319,
            "start_X": 420,
            "start_Y": 418,
            "end_X": 420,
            "end_Y": 449,
            "mapUnitId": 55,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 320,
            "start_X": 420,
            "start_Y": 449,
            "end_X": 388,
            "end_Y": 449,
            "mapUnitId": 55,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 321,
            "start_X": 388,
            "start_Y": 449,
            "end_X": 388,
            "end_Y": 418,
            "mapUnitId": 55,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 56,
        "unitName": "2A143",
        "lines": [
          {
            "Id": 322,
            "start_X": 357,
            "start_Y": 418,
            "end_X": 388,
            "end_Y": 418,
            "mapUnitId": 56,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 323,
            "start_X": 388,
            "start_Y": 418,
            "end_X": 388,
            "end_Y": 449,
            "mapUnitId": 56,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 324,
            "start_X": 388,
            "start_Y": 449,
            "end_X": 357,
            "end_Y": 449,
            "mapUnitId": 56,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 325,
            "start_X": 357,
            "start_Y": 449,
            "end_X": 357,
            "end_Y": 418,
            "mapUnitId": 56,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 57,
        "unitName": "2A141",
        "lines": [
          {
            "Id": 326,
            "start_X": 326,
            "start_Y": 418,
            "end_X": 357,
            "end_Y": 418,
            "mapUnitId": 57,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 327,
            "start_X": 357,
            "start_Y": 418,
            "end_X": 357,
            "end_Y": 449,
            "mapUnitId": 57,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 328,
            "start_X": 357,
            "start_Y": 449,
            "end_X": 326,
            "end_Y": 449,
            "mapUnitId": 57,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 329,
            "start_X": 326,
            "start_Y": 449,
            "end_X": 326,
            "end_Y": 418,
            "mapUnitId": 57,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 58,
        "unitName": "2A137",
        "lines": [
          {
            "Id": 330,
            "start_X": 263,
            "start_Y": 422,
            "end_X": 295,
            "end_Y": 422,
            "mapUnitId": 58,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 331,
            "start_X": 295,
            "start_Y": 422,
            "end_X": 295,
            "end_Y": 449,
            "mapUnitId": 58,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 332,
            "start_X": 295,
            "start_Y": 449,
            "end_X": 263,
            "end_Y": 449,
            "mapUnitId": 58,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 333,
            "start_X": 263,
            "start_Y": 449,
            "end_X": 263,
            "end_Y": 422,
            "mapUnitId": 58,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 59,
        "unitName": "2A136",
        "lines": [
          {
            "Id": 334,
            "start_X": 232,
            "start_Y": 422,
            "end_X": 263,
            "end_Y": 422,
            "mapUnitId": 59,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 335,
            "start_X": 263,
            "start_Y": 422,
            "end_X": 263,
            "end_Y": 449,
            "mapUnitId": 59,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 336,
            "start_X": 263,
            "start_Y": 449,
            "end_X": 232,
            "end_Y": 449,
            "mapUnitId": 59,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 337,
            "start_X": 232,
            "start_Y": 449,
            "end_X": 232,
            "end_Y": 422,
            "mapUnitId": 59,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 60,
        "unitName": "2A139",
        "lines": [
          {
            "Id": 338,
            "start_X": 300,
            "start_Y": 418,
            "end_X": 326,
            "end_Y": 418,
            "mapUnitId": 60,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 339,
            "start_X": 326,
            "start_Y": 418,
            "end_X": 326,
            "end_Y": 449,
            "mapUnitId": 60,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 340,
            "start_X": 326,
            "start_Y": 449,
            "end_X": 295,
            "end_Y": 449,
            "mapUnitId": 60,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 341,
            "start_X": 295,
            "start_Y": 449,
            "end_X": 295,
            "end_Y": 422,
            "mapUnitId": 60,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 342,
            "start_X": 295,
            "start_Y": 422,
            "end_X": 300,
            "end_Y": 418,
            "mapUnitId": 60,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 61,
        "unitName": "2A501",
        "lines": [
          {
            "Id": 343,
            "start_X": 461,
            "start_Y": 374,
            "end_X": 482,
            "end_Y": 374,
            "mapUnitId": 61,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 344,
            "start_X": 482,
            "start_Y": 374,
            "end_X": 482,
            "end_Y": 354,
            "mapUnitId": 61,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 345,
            "start_X": 482,
            "start_Y": 354,
            "end_X": 461,
            "end_Y": 354,
            "mapUnitId": 61,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 346,
            "start_X": 461,
            "start_Y": 354,
            "end_X": 461,
            "end_Y": 346,
            "mapUnitId": 61,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 347,
            "start_X": 461,
            "start_Y": 346,
            "end_X": 419,
            "end_Y": 346,
            "mapUnitId": 61,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 348,
            "start_X": 419,
            "start_Y": 346,
            "end_X": 419,
            "end_Y": 376,
            "mapUnitId": 61,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 349,
            "start_X": 419,
            "start_Y": 376,
            "end_X": 461,
            "end_Y": 376,
            "mapUnitId": 61,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 350,
            "start_X": 461,
            "start_Y": 376,
            "end_X": 461,
            "end_Y": 374,
            "mapUnitId": 61,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 62,
        "unitName": "2A148",
        "lines": [
          {
            "Id": 351,
            "start_X": 461,
            "start_Y": 374,
            "end_X": 482,
            "end_Y": 374,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 352,
            "start_X": 482,
            "start_Y": 374,
            "end_X": 482,
            "end_Y": 387,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 353,
            "start_X": 482,
            "start_Y": 387,
            "end_X": 482,
            "end_Y": 407,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 354,
            "start_X": 482,
            "start_Y": 407,
            "end_X": 482,
            "end_Y": 415,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 355,
            "start_X": 482,
            "start_Y": 415,
            "end_X": 482,
            "end_Y": 423,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 356,
            "start_X": 482,
            "start_Y": 423,
            "end_X": 472,
            "end_Y": 423,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 357,
            "start_X": 472,
            "start_Y": 423,
            "end_X": 472,
            "end_Y": 415,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 358,
            "start_X": 472,
            "start_Y": 415,
            "end_X": 472,
            "end_Y": 407,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 359,
            "start_X": 472,
            "start_Y": 407,
            "end_X": 461,
            "end_Y": 407,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 360,
            "start_X": 461,
            "start_Y": 407,
            "end_X": 444,
            "end_Y": 407,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 361,
            "start_X": 444,
            "start_Y": 407,
            "end_X": 419,
            "end_Y": 407,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 362,
            "start_X": 419,
            "start_Y": 407,
            "end_X": 419,
            "end_Y": 376,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 363,
            "start_X": 419,
            "start_Y": 376,
            "end_X": 444,
            "end_Y": 376,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 364,
            "start_X": 444,
            "start_Y": 376,
            "end_X": 461,
            "end_Y": 376,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 365,
            "start_X": 461,
            "start_Y": 376,
            "end_X": 461,
            "end_Y": 374,
            "mapUnitId": 62,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 63,
        "unitName": "2A504",
        "lines": [
          {
            "Id": 366,
            "start_X": 404,
            "start_Y": 346,
            "end_X": 370,
            "end_Y": 346,
            "mapUnitId": 63,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 367,
            "start_X": 370,
            "start_Y": 346,
            "end_X": 370,
            "end_Y": 314,
            "mapUnitId": 63,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 368,
            "start_X": 370,
            "start_Y": 314,
            "end_X": 409,
            "end_Y": 314,
            "mapUnitId": 63,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 369,
            "start_X": 409,
            "start_Y": 314,
            "end_X": 409,
            "end_Y": 340,
            "mapUnitId": 63,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 370,
            "start_X": 409,
            "start_Y": 340,
            "end_X": 404,
            "end_Y": 346,
            "mapUnitId": 63,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 64,
        "unitName": "2A509",
        "lines": [
          {
            "Id": 371,
            "start_X": 390,
            "start_Y": 190,
            "end_X": 424,
            "end_Y": 190,
            "mapUnitId": 64,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 372,
            "start_X": 424,
            "start_Y": 190,
            "end_X": 424,
            "end_Y": 229,
            "mapUnitId": 64,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 373,
            "start_X": 424,
            "start_Y": 229,
            "end_X": 390,
            "end_Y": 229,
            "mapUnitId": 64,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 374,
            "start_X": 390,
            "start_Y": 229,
            "end_X": 390,
            "end_Y": 222,
            "mapUnitId": 64,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 375,
            "start_X": 390,
            "start_Y": 222,
            "end_X": 387,
            "end_Y": 220,
            "mapUnitId": 64,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 376,
            "start_X": 387,
            "start_Y": 220,
            "end_X": 387,
            "end_Y": 202,
            "mapUnitId": 64,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 377,
            "start_X": 387,
            "start_Y": 202,
            "end_X": 390,
            "end_Y": 201,
            "mapUnitId": 64,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 378,
            "start_X": 390,
            "start_Y": 201,
            "end_X": 390,
            "end_Y": 190,
            "mapUnitId": 64,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 65,
        "unitName": "2A622",
        "lines": [
          {
            "Id": 379,
            "start_X": 457,
            "start_Y": 190,
            "end_X": 424,
            "end_Y": 190,
            "mapUnitId": 65,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 380,
            "start_X": 424,
            "start_Y": 190,
            "end_X": 424,
            "end_Y": 229,
            "mapUnitId": 65,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 381,
            "start_X": 424,
            "start_Y": 229,
            "end_X": 457,
            "end_Y": 229,
            "mapUnitId": 65,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 382,
            "start_X": 457,
            "start_Y": 229,
            "end_X": 457,
            "end_Y": 222,
            "mapUnitId": 65,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 383,
            "start_X": 457,
            "start_Y": 222,
            "end_X": 460,
            "end_Y": 220,
            "mapUnitId": 65,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 384,
            "start_X": 460,
            "start_Y": 220,
            "end_X": 460,
            "end_Y": 202,
            "mapUnitId": 65,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 385,
            "start_X": 460,
            "start_Y": 202,
            "end_X": 457,
            "end_Y": 201,
            "mapUnitId": 65,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 386,
            "start_X": 457,
            "start_Y": 201,
            "end_X": 457,
            "end_Y": 190,
            "mapUnitId": 65,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 66,
        "unitName": "2A620",
        "lines": [
          {
            "Id": 387,
            "start_X": 457,
            "start_Y": 267,
            "end_X": 424,
            "end_Y": 267,
            "mapUnitId": 66,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 388,
            "start_X": 424,
            "start_Y": 267,
            "end_X": 424,
            "end_Y": 229,
            "mapUnitId": 66,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 389,
            "start_X": 424,
            "start_Y": 229,
            "end_X": 457,
            "end_Y": 229,
            "mapUnitId": 66,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 390,
            "start_X": 457,
            "start_Y": 229,
            "end_X": 457,
            "end_Y": 235,
            "mapUnitId": 66,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 391,
            "start_X": 457,
            "start_Y": 235,
            "end_X": 460,
            "end_Y": 237,
            "mapUnitId": 66,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 392,
            "start_X": 460,
            "start_Y": 237,
            "end_X": 460,
            "end_Y": 256,
            "mapUnitId": 66,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 393,
            "start_X": 460,
            "start_Y": 256,
            "end_X": 457,
            "end_Y": 256,
            "mapUnitId": 66,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 394,
            "start_X": 457,
            "start_Y": 256,
            "end_X": 457,
            "end_Y": 267,
            "mapUnitId": 66,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 67,
        "unitName": "2A619",
        "lines": [
          {
            "Id": 395,
            "start_X": 457,
            "start_Y": 267,
            "end_X": 508,
            "end_Y": 267,
            "mapUnitId": 67,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 396,
            "start_X": 508,
            "start_Y": 267,
            "end_X": 508,
            "end_Y": 229,
            "mapUnitId": 67,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 397,
            "start_X": 508,
            "start_Y": 229,
            "end_X": 475,
            "end_Y": 229,
            "mapUnitId": 67,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 398,
            "start_X": 475,
            "start_Y": 229,
            "end_X": 475,
            "end_Y": 235,
            "mapUnitId": 67,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 399,
            "start_X": 475,
            "start_Y": 235,
            "end_X": 471,
            "end_Y": 237,
            "mapUnitId": 67,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 400,
            "start_X": 471,
            "start_Y": 237,
            "end_X": 471,
            "end_Y": 256,
            "mapUnitId": 67,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 401,
            "start_X": 471,
            "start_Y": 256,
            "end_X": 475,
            "end_Y": 256,
            "mapUnitId": 67,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 402,
            "start_X": 475,
            "start_Y": 256,
            "end_X": 457,
            "end_Y": 267,
            "mapUnitId": 67,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 68,
        "unitName": "2A617",
        "lines": [
          {
            "Id": 403,
            "start_X": 475,
            "start_Y": 267,
            "end_X": 508,
            "end_Y": 267,
            "mapUnitId": 68,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 404,
            "start_X": 508,
            "start_Y": 267,
            "end_X": 508,
            "end_Y": 293,
            "mapUnitId": 68,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 405,
            "start_X": 508,
            "start_Y": 293,
            "end_X": 471,
            "end_Y": 293,
            "mapUnitId": 68,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 406,
            "start_X": 471,
            "start_Y": 293,
            "end_X": 471,
            "end_Y": 276,
            "mapUnitId": 68,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 407,
            "start_X": 471,
            "start_Y": 276,
            "end_X": 475,
            "end_Y": 276,
            "mapUnitId": 68,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 408,
            "start_X": 475,
            "start_Y": 276,
            "end_X": 475,
            "end_Y": 267,
            "mapUnitId": 68,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 69,
        "unitName": "2A621",
        "lines": [
          {
            "Id": 409,
            "start_X": 475,
            "start_Y": 190,
            "end_X": 508,
            "end_Y": 190,
            "mapUnitId": 69,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 410,
            "start_X": 508,
            "start_Y": 190,
            "end_X": 508,
            "end_Y": 229,
            "mapUnitId": 69,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 411,
            "start_X": 508,
            "start_Y": 229,
            "end_X": 475,
            "end_Y": 229,
            "mapUnitId": 69,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 412,
            "start_X": 475,
            "start_Y": 229,
            "end_X": 475,
            "end_Y": 222,
            "mapUnitId": 69,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 413,
            "start_X": 475,
            "start_Y": 222,
            "end_X": 471,
            "end_Y": 220,
            "mapUnitId": 69,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 414,
            "start_X": 471,
            "start_Y": 220,
            "end_X": 471,
            "end_Y": 202,
            "mapUnitId": 69,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 415,
            "start_X": 471,
            "start_Y": 202,
            "end_X": 475,
            "end_Y": 201,
            "mapUnitId": 69,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 416,
            "start_X": 475,
            "start_Y": 201,
            "end_X": 475,
            "end_Y": 190,
            "mapUnitId": 69,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 70,
        "unitName": "2A507",
        "lines": [
          {
            "Id": 417,
            "start_X": 390,
            "start_Y": 267,
            "end_X": 424,
            "end_Y": 267,
            "mapUnitId": 70,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 418,
            "start_X": 424,
            "start_Y": 267,
            "end_X": 424,
            "end_Y": 229,
            "mapUnitId": 70,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 419,
            "start_X": 424,
            "start_Y": 229,
            "end_X": 390,
            "end_Y": 229,
            "mapUnitId": 70,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 420,
            "start_X": 390,
            "start_Y": 229,
            "end_X": 390,
            "end_Y": 235,
            "mapUnitId": 70,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 421,
            "start_X": 390,
            "start_Y": 235,
            "end_X": 387,
            "end_Y": 237,
            "mapUnitId": 70,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 422,
            "start_X": 387,
            "start_Y": 237,
            "end_X": 387,
            "end_Y": 256,
            "mapUnitId": 70,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 423,
            "start_X": 387,
            "start_Y": 256,
            "end_X": 390,
            "end_Y": 256,
            "mapUnitId": 70,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 424,
            "start_X": 390,
            "start_Y": 256,
            "end_X": 390,
            "end_Y": 267,
            "mapUnitId": 70,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 71,
        "unitName": "2A505",
        "lines": [
          {
            "Id": 425,
            "start_X": 390,
            "start_Y": 267,
            "end_X": 424,
            "end_Y": 267,
            "mapUnitId": 71,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 426,
            "start_X": 424,
            "start_Y": 267,
            "end_X": 424,
            "end_Y": 229,
            "mapUnitId": 71,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 427,
            "start_X": 424,
            "start_Y": 229,
            "end_X": 390,
            "end_Y": 229,
            "mapUnitId": 71,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 428,
            "start_X": 390,
            "start_Y": 229,
            "end_X": 390,
            "end_Y": 235,
            "mapUnitId": 71,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 429,
            "start_X": 390,
            "start_Y": 235,
            "end_X": 387,
            "end_Y": 237,
            "mapUnitId": 71,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 430,
            "start_X": 387,
            "start_Y": 237,
            "end_X": 387,
            "end_Y": 256,
            "mapUnitId": 71,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 431,
            "start_X": 387,
            "start_Y": 256,
            "end_X": 390,
            "end_Y": 256,
            "mapUnitId": 71,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 432,
            "start_X": 390,
            "start_Y": 256,
            "end_X": 390,
            "end_Y": 267,
            "mapUnitId": 71,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      },
      {
        "map": {
          "BeaconProfiles": [],
          "Id": 1,
          "Name": "Conestoga-A wing"
        },
        "Id": 72,
        "unitName": "2A618",
        "lines": [
          {
            "Id": 433,
            "start_X": 457,
            "start_Y": 267,
            "end_X": 424,
            "end_Y": 267,
            "mapUnitId": 72,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 434,
            "start_X": 424,
            "start_Y": 267,
            "end_X": 424,
            "end_Y": 300,
            "mapUnitId": 72,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 435,
            "start_X": 424,
            "start_Y": 300,
            "end_X": 459,
            "end_Y": 300,
            "mapUnitId": 72,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 436,
            "start_X": 459,
            "start_Y": 300,
            "end_X": 460,
            "end_Y": 293,
            "mapUnitId": 72,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 437,
            "start_X": 460,
            "start_Y": 293,
            "end_X": 460,
            "end_Y": 277,
            "mapUnitId": 72,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 438,
            "start_X": 460,
            "start_Y": 277,
            "end_X": 450,
            "end_Y": 276,
            "mapUnitId": 72,
            "color": "black",
            "stroke_width": 1
          },
          {
            "Id": 439,
            "start_X": 450,
            "start_Y": 276,
            "end_X": 457,
            "end_Y": 267,
            "mapUnitId": 72,
            "color": "black",
            "stroke_width": 1
          }
        ],
        "circles": [],
        "MapId": 1
      }
    ],
    "BeaconProfiles": []
  }
};