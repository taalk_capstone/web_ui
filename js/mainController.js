var app = angular.module('app', []);

app.controller('mainController', [ '$scope', '$log', 'mapInfoService', 'webSocketService', 'beaconInfoService', 'deviceInfoService', 'heatAreaInfoService', 'nodeInfoService', 'navigationService', 'searchInfoService','$rootScope', '$interval', function($scope, $log, mapInfoService, webSocketService, beaconInfoService, deviceInfoService, heatAreaInfoService, nodeInfoService, navigationService, searchInfoService, $rootScope, $interval) {
    //group info
    $scope.groupName = "TAALK";
    $scope.groupMembers = "Tiago, Ady, Alex, Lalit and Kevin";
        //variables
    $scope.devices = [];
    var nodeList = [];
    
    var textToSpeech = "";
    
    //list of colors to use in each connected device
    var colorList = ["#1abc9c", "#9b59b6", "#f39c12", "#2ecc71", "#3498db", "#e74c3c"];
    
    //SVG content
    var width = window.innerWidth;
    var height = window.innerHeight;

    var randomX = d3.random.normal(width / 2, 80),
        randomY = d3.random.normal(height / 2, 80);

    var offset = 0;

    if(width > 774)
    {
        offset = -(width - 774)/2;
    }   
    else{
        offset = (774 - width)/2
    }

    var svg = d3.select("#mainpage").append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", offset + " 0 " + width + " " + height)
      .append("g")
        .call(d3.behavior.zoom().scaleExtent([1, 8]).on("zoom", zoom))
      ;
    
    //border
    svg.append("polygon")
        .attr("points", "182,625 207,625 207,0 327,0 327,40 263,40 263,147 353,147 352,75 375,75 375,70 414,70 414,75 446,75 446,63 455,63 456,75 491,75 496,109 496,125 774,125 774,835 526,835 423,835 423,842 404,842 404,835 101,835 101,785 87,785 56,813 56,823 51,823 51,835 51,842 24,842 24,839 12,839 12,820 0,800 0,757 136,620 156,600")
        .attr("stroke", "none")
        .attr("fill",  "#bdc3c7")
        .attr("fill-opacity", 0.1);
    
    
    $scope.dataLoading = true;
    // websocket connection
    webSocketService.connect();
    
    //get map 
    $rootScope.$on('UpdateMap', function(){
        updateMap();
    });
    
    $rootScope.$on('UpdateBeacon', function(){
        updateBeacons();
    });
    
    $rootScope.$on('UpdateHeatArea', function(){
        updateHeatAreas();
    });
    
    $rootScope.$on('UpdateNode', function(){
        updateNodes();
    });
    
    $rootScope.$on('UpdateDevice', function(){
        updateDevices();
    });
    
    $rootScope.$on('UpdateSearch', function(){
        updateSearch();
    });

    function zoom() {
      svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }
    
    function updateWindow(){
        var x = window.innerWidth;
        var y = window.innerHeight;
        svg.attr("width", x).attr("height", y);
    }
    
    window.onresize = updateWindow;   

    function updateMap(){
        $scope.dataLoading = true;

         //define mapUnit
         var mapUnit = {
            Id : 0,
            unitName : "",
            lines : [],
            cricles : []
         }
         
           //lines
         mapUnits = mapInfoService.getMapUnits();
         var nSize = mapUnits.length;
        //all classrooms
        var group = svg.append("g")
                .attr("id", "classrooms");
         for (var i = 0; i < nSize; i++) {
            var mapUnit = mapUnits[i];
            
             //each classroom
             var classroom = group.append("g")
                .attr("id", mapUnit.unitName);
            //each classroom has a group of lines
            lines = mapUnit.lines;
            lines.forEach(function(line){
                
                classroom.append("line")
                     .attr("x1", line.start_X)
                     .attr("y1", line.start_Y)
                     .attr("x2", line.end_X)
                     .attr("y2", line.end_Y)
                     .attr("stroke-width", line.stroke_width)
                     .attr("stroke", line.color);
            });
         };
        
        //apply all the changes, draw the map
        $scope.dataLoading = false;
        $scope.$apply();

        if (webSocketService.getState()){
            //request the beacons
             var request = {
                    commandName : "601",
                    commandParams: {id : "1" }
                }
             webSocketService.send(JSON.stringify(request));
            
            if (webSocketService.getState()) {
                //request nodes
                 var request = {
                        commandName : "1001",
                        commandParams: {id : "1" }
                    }
                 webSocketService.send(JSON.stringify(request));
            }
        }
    }
    
    function updateBeacons(){
        $scope.dataLoading = true;
        var beaconList = beaconInfoService.getBeaconInfo();
        
        if (beaconList != null && beaconList.length > 0){
            $scope.beacons = beaconList;
        }
        
        var group = svg.append("g")
                .attr("id", "beacons");
        beaconList.forEach(function(beacon){
            
            var link = group.append("a")
                            .attr("href", "#");

            var title = link.append("title").text(beacon.minor);
            
            var circle = link.append("circle");
                
            circle.attr("id", beacon.id)
                .attr("name", beacon.minor)
                 .attr("cx", beacon.Pt_X)
                 .attr("cy", beacon.Pt_Y)
                .attr("r", beacon.height)
                 .attr("stroke-width", 0.4)
                 .attr("stroke", "#c0392b")
                .attr("fill", "#c0392b");       
            });
        $scope.dataLoading = false;
        $scope.$apply();
        
    }
    
    function updateHeatAreas(){
        $scope.dataLoading = true;
        var heatAreaList = heatAreaInfoService.getHeatAreaInfo();
        var group = svg.append("g")
                .attr("id", "heatAreas");
        heatAreaList.forEach(function(heatArea){
                
            if (heatArea.radius < 45 ){
                var circle = group.append("circle")
                    .attr("id", heatArea.id)
                    .attr("name", heatArea.beaconId)
                     .attr("cx", heatArea.center_X)
                     .attr("cy", heatArea.center_Y)
                     .attr("r", heatArea.radius)
                     .attr("stroke-width", 0.4) ;
                if (heatArea.radius == 22){ //zone 1
                   circle.attr("stroke", "#bdc3c7")
                   .attr("stroke-opacity", 0.1)
                   .attr("fill-opacity", 0.1)
                    .attr("fill", "#bdc3c7");
                } else if (heatArea.radius == 44){ //zone 2
                     circle.attr("stroke", "#95a5a6")
                     .attr("stroke-opacity", 0.3)
                     .attr("fill-opacity", 0.3)
                    .attr("fill", "#95a5a6");
                }
            }

        });
        $scope.dataLoading = false;
        $scope.$apply();
    
    }
    
    $scope.timer = $interval(function () {
        cleanDeviceList();
    }, 10000);
    
    function cleanDeviceList(){
        var now = new Date();
        var devTime = 1470169866969;
        for (var i = 0; i < $scope.devices.length; i++){
            var dev = $scope.devices[i];
            devTime = dev.time.getTime();
            if ((devTime + 20000) < now.getTime()){
                console.info('now: ' + now);  
                console.info('dev: ' + devTime);
                console.info('diff: ' + (now - devTime));
                $scope.devices.splice(i, 1);
                //delete the device from the view
                svg.selectAll("#D" + dev.id).remove();
            }
        } 
    }
    
    $scope.showInfo = false;
    var colorIndex = 0;
    function updateDevices(){
        var deviceInfo = deviceInfoService.getDeviceInfo();
        var device = deviceInfoService.getDeviceProfile();
        
        if (device != null){
            
            //populate the list of devices
            device.time = new Date();
            
            if ($scope.devices.length == 0){
                $scope.devices.push(device);
            }
            
            var exists = false;
            for (var i = 0; i < $scope.devices.length; i++){
                var dev = $scope.devices[i];
                if (dev.id == device.id){
                    exists = true;
                    colorIndex = i % $scope.devices.length;
                    dev.userName = device.userName;
                }
            }
            
            if (exists == false){
                $scope.devices.push(device);
                colorIndex = ($scope.devices.length - 1) % $scope.devices.length;
            }
            
            //text to speech
            var text = deviceInfoService.getNotificationText();
            if (text != null && text != textToSpeech){
                speak(text);
                textToSpeech = text;
            }
        }
        
        drawDevices();
    }
    
    function drawDevices(){
        
        //create a group to hide using settings menu
        var group = svg.append("g")
            .attr("id", "devices");
        
        //draw the devices
        if ($scope.devices.length > 0) {

            var heatAreas = deviceInfoService.getHeatArea();

            var middleDeviceX = 0;
            var middleDeviceY = 0;
            var zone1 = false;

            heatAreas.forEach(function(heatArea){
                if (heatArea.radius < 23){
                    middleDeviceX = heatArea.center_X;
                    middleDeviceY = heatArea.center_Y;
                    zone1 = true;
                }
            });

            if (zone1 == false){
                heatAreas.forEach(function(heatArea){
                    middleDeviceX += heatArea.center_X / heatAreas.length;
                    middleDeviceY += heatArea.center_Y / heatAreas.length;
                });
            } 
            
            var node = deviceInfoService.getNode();
            if (node != null){
                middleDeviceX = node.center_X;
                middleDeviceY = node.center_Y;

                for (var i = 0; i< $scope.devices.length; i++){

                    var device = $scope.devices[i];

                    //clean the drawing first
                    svg.selectAll("#D" + device.id).remove();
                    
                    var devices = group.append("g")
                                .attr("id", "D" + device.id);

                    //data related to heat area
                    var circle = group.append("circle")
                        .attr("stroke", colorList[colorIndex])
                        .attr("stroke-opacity", 0.8)
                        .attr("fill-opacity", 0.8)
                        .attr("fill", colorList[colorIndex])
                        .attr("cx", middleDeviceX)
                        .attr("cy", middleDeviceY)
                        .on("click", function(){
                            showDeviceInfo(device);
                        });

                        if (zone1 == true){
                            circle.attr("r", 22);  
                        } else {
                            circle.attr("r", 44);  
                        }

                        var link = group.append("a")
                                        .attr("href", "#");

                        var title = link.append("title").text(device.userName);

                        var image = link.append("image")
                            .attr("x", middleDeviceX - 12) //centralize image
                             .attr("y", middleDeviceY - 12)
                             .attr("width", 22)
                             .attr("height", 22)
                             .attr("xlink:href", "img/iphone_32x32.png")
                            .on("click", function(){
                                showDeviceInfo(device);
                            });
                }
            }
        }
        $scope.$apply();
    }
    
    function updateNodes(){
        $scope.dataLoading = true;
        nodeList = nodeInfoService.getNodeInfo();
        var group = svg.append("g")
                .attr("id", "nodes");
        nodeList.forEach(function(node){
                
            group.append("circle")
                .attr("id", node.id)
                .attr("name", node.name)
                 .attr("cx", node.center_X)
                 .attr("cy", node.center_Y)
                .attr("r", 2) //node.radius)
                 .attr("stroke-width", 2)
                 .attr("stroke", "#27ae60")
                .attr("fill", "#27ae60");
                
            });  
        
        $scope.$apply();
        $scope.dataLoading = false;
        
    }
    
    function showDeviceInfo(device) {
        $scope.showInfo = true;
        $scope.device = device;
        
        openCloseInfo($scope.showInfo);
    };
    
    $scope.showDevInfo = function(device) {
        showDeviceInfo(device);
    };
    
    $rootScope.search = function(){
        $scope.dataLoading = true;
        var text = $("#search").val();
        
        if (text != null && text.trim().length > 0){
            
            speak('Finding directions to ' + text);

            if (webSocketService.getState()) {
                //request the search
                 var request = {
                        commandName : "1301",
                        commandParams: {nodeName : text }
                    }
                 webSocketService.send(JSON.stringify(request));
            } else {
                //server is not connect
            }
        } else {
            var alert = 'Please select a place to navigate.';
            speak(alert);
        }
            
         $scope.dataLoading = false;
            
    }
    
    function updateSearch(){
        $scope.dataLoading = true;
        
        if($('#navigationCheckbox').prop('checked')) {
        
            var path = searchInfoService.getSearchInfo();

            //remove previous path before drawing the next
            svg.selectAll("#navigationPath").remove();

            if (path != null){
                navigationService.navigate(nodeList, path, svg);
                
            } else{
                speak('Sorry, the place is not reachable. Try another place.');
            }
        } else {
            //alert('Navigation is not enabled.');
        }
        
        $scope.dataLoading = false;
    }
    
    $scope.dataLoading = false;
 
    // say a message
    function speak(text, callback) {
        var u = new SpeechSynthesisUtterance();
        u.text = text;
        u.lang = 'en-US';

        u.onend = function () {
            if (callback) {
                callback();
            }
        };

        u.onerror = function (e) {
            if (callback) {
                callback(e);
            }
        };

        speechSynthesis.speak(u);
    }

}]);

app.directive('tooltip', function(){
    return {
        restrict: 'E',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});