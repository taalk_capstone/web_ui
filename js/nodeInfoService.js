//NodeInfoService.js
'use strict';
app.factory('nodeInfoService',  function () {

	 var nodeInfoServiceFactory = {};

	 var nodeInfo = {
	 	MapId:0,
        center_X:165,
        center_Y:640,
        description:"close to door 3",
        id:1,
        name:"1",
        radius:1,
	 };

	 var _setNodeInfo = function(nodeInfoData){

	 	nodeInfo = nodeInfoData;
	 } 

	 var _getNodeInfo = function(){

	 	return nodeInfo;
	 }

	 nodeInfoServiceFactory.setNodeInfo = _setNodeInfo;
	 nodeInfoServiceFactory.getNodeInfo = _getNodeInfo;

	 return nodeInfoServiceFactory;

});