//webSocketService.js
'use strict';
app.factory('webSocketService', ['messageHandlerService','$rootScope',  function ( messageHandlerService, $rootScope) {

    var webSocket;
    
    var state = false;
    
    var Service = {};
            
    Service.connect = function(){
        var address = "WS://192.168.201.1:54321";
        webSocket = new WebSocket(address);

        webSocket.onopen = function(e) {
            changeState(true);
            console.log("Connection open...");

            //first request the map
             var request = {
                    commandName : "101",
                    commandParams: {id : "1" }
                }
             webSocket.send(JSON.stringify(request));

        };

        webSocket.onerror = function (e) {
            changeState(false);
            console.log("Connection error...");
            
        };

        webSocket.onmessage = function(e){
            if(typeof e.data === "string")
            {    
                //console.log("Received : " + e.data);
                messageHandlerService.process(JSON.parse(e.data));
            }
            else{
                console.log("Binary message received...");
            }   
        };

        webSocket.onclose = function(e){
            console.log("Connection Closed...");
            changeState(false);
            
            Service.connect();
        };
    };

    Service.disconnect = function(){
        console.log("Closing connection...");
        webSocket.close();
    };

    var log = function(message){
        var text = document.createTextNode(message);
    };
        
    var changeState = function(isConnected){
        state = isConnected;
    };
    
    Service.getState = function(){
        return state;
    }

    Service.send = function(request) {
        if (webSocket.readyState != 1) {
            log("Cannot send data when the connection is closed...");
            return false;
        }
        console.log("Sending : " + request);
        webSocket.send(request);
        
        return true;
    };
    
    return Service;

}]);