//MapInfoService.js
'use strict';
app.factory('mapInfoService',  function () {

	 var mapInfoServiceFactory = {};

	 //define line 
	 var line = {
	 	Id: 0 ,
	 	start_X : 0,
	 	start_Y : 0,
	 	end_X : 0,
	 	end_Y : 0,
	 	color : "",
	 	stroke_width: ""
	 }

	 //define mapUnit
	 var mapUnit = {
	 	Id : 0,
	 	unitName : "",
	 	lines : [],
	 	cricles : [], 
	 }

	 //define mapInfo
	 var mapInfo = {
	 	Id: 0,
  		Name : '',
  		MapUnits : [], 
  		BeaconProfiles : []
	 };

	 var _setMapInfo = function(mapInfoData){
	 	mapInfo = mapInfoData;
	 } 

	 var _getMapUnits = function(){
	 	return mapInfo.MapUnits;
	 }

	 var _getMapInfo = function(){
	 	return mapInfo;
	 }


	 mapInfoServiceFactory.setMapInfo = _setMapInfo;
	 mapInfoServiceFactory.getMapInfo = _getMapInfo;
	 mapInfoServiceFactory.getMapUnits = _getMapUnits;
	 
	 return mapInfoServiceFactory;

});