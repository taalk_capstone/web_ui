'use strict';
app.factory('messageHandlerService', ['mapInfoService', 'beaconInfoService', 'heatAreaInfoService', 'nodeInfoService', 'deviceInfoService', 'searchInfoService', '$rootScope', function (mapInfoService, beaconInfoService, heatAreaInfoService, nodeInfoService, deviceInfoService, searchInfoService, $rootScope) {

    var messageHandlerFactory = {};

    messageHandlerFactory.process = function (data) {
        //when receiving a message from the server
        switch (data['commandName']){
            case '102':
            {
                if(data['commandResult'] == 'Succeed'){
                    mapInfoService.setMapInfo(data['resource']);    
                    $rootScope.$broadcast('UpdateMap');
                }
                
            }
                break;
            case '602':
            {
                if(data['commandResult'] == 'Succeed'){
                    beaconInfoService.setBeaconInfo(data['resource']);    
                    $rootScope.$broadcast('UpdateBeacon');
                }
                
            }
                break;
            case '1102':
            {
                if(data['commandResult'] == 'Succeed'){
                    heatAreaInfoService.setHeatAreaInfo(data['resource']);    
                    $rootScope.$broadcast('UpdateHeatArea');
                }
                
            }
                break;
            case '802':
            {
                if(data['commandResult'] == 'Succeed'){
                    deviceInfoService.setDeviceInfo(data['resource']);    
                    $rootScope.$broadcast('UpdateDevice');
                }
                
            }
                break;
            case '1002':
            {
                if(data['commandResult'] == 'Succeed'){
                    nodeInfoService.setNodeInfo(data['resource']);    
                    $rootScope.$broadcast('UpdateNode');
                }
                
            }
                break;
            case '902':
            {
                if(data['commandResult'] == 'Succeed'){
                    deviceInfoService.setDeviceInfo(data['resource']);    
                    $rootScope.$broadcast('UpdateDevice');
                }
                
            }
                break;
            case '1302':
            {
                searchInfoService.setSearchInfo(data['resource']);    
                $rootScope.$broadcast('UpdateSearch');
                
            }
                break;
            default:
                break;
        }
    }

    return messageHandlerFactory;

}]);