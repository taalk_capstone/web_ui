//DeviceInfoService.js
'use strict';
app.factory('deviceInfoService',  function () {

	 var deviceInfoServiceFactory = {};

	 var deviceInfo = {
	 	 deviceProfile: {},
         heatAreasList: [],
         node: {},
         timestamp: "",
         notificationText: ""
	 };

	 var devicePoints = []; 

	 var _setDeviceInfo = function(deviceInfoData){

	 	deviceInfo = deviceInfoData;
	 } 

	 var _getDeviceInfo = function(){

	 	return [deviceInfo];
	 }
     
     var _getDeviceProfile = function(){

	 	return deviceInfo.deviceProfile;
	 }
     
     var _getHeatArea = function(){

	 	return deviceInfo.heatAreasList;
	 }
     
    var _getNode = function(){

	 	return deviceInfo.node;
	 }
    
    var _getNotificationText = function(){

	 	return deviceInfo.notificationText;
	 }

	 deviceInfoServiceFactory.setDeviceInfo = _setDeviceInfo;
	 deviceInfoServiceFactory.getDeviceInfo = _getDeviceInfo;
    deviceInfoServiceFactory.getDeviceProfile = _getDeviceProfile;
    deviceInfoServiceFactory.getHeatArea = _getHeatArea;
    deviceInfoServiceFactory.getNode = _getNode;
    deviceInfoServiceFactory.getNotificationText = _getNotificationText;

	 return deviceInfoServiceFactory;

});