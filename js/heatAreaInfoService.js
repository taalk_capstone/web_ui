//HeatAreaInfoService.js
'use strict';
app.factory('heatAreaInfoService',  function () {

	 var heatAreaInfoServiceFactory = {};

	  var heatAreaInfo = {
        beaconId:1,
        center_X:165,
        center_Y:642,
        id:1,
        mapId:1,
        radius:22
    };

	 var _setHeatAreaInfo = function(heatAreaInfoData){

	 	heatAreaInfo = heatAreaInfoData;
	 } 

	 var _getHeatAreaInfo = function(){

	 	return heatAreaInfo;
	 }

	 heatAreaInfoServiceFactory.setHeatAreaInfo = _setHeatAreaInfo;
	 heatAreaInfoServiceFactory.getHeatAreaInfo = _getHeatAreaInfo;

	 return heatAreaInfoServiceFactory;

});