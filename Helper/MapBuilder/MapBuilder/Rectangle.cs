﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapBuilder
{
    class Rectangle
    {

        private String id;

        public String Id
        {
            get { return id; }
            set { id = value; }
        }

        public Rectangle()
        {
            point = new Point();
        }

        private Point point;

        public Point Point
        {
            get { return point; }
            set { point = value; }
        }
        private int width;

        public int Width
        {
            get { return width; }
            set { width = value; }
        }
        private int height;

        public int Height
        {
            get { return height; }
            set { height = value; }
        }

        public override string ToString()
        {
            return "Rectangle " + id + ": x= " + point.X + " y= " + point.Y;
        }
    }
}
