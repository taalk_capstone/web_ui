﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapBuilder
{
    class Circle
    {

        public Circle()
        {
            point = new Point();
        }

        private Point point;

        public Point Point
        {
            get { return point; }
            set { point = value; }
        }
        private int r;

        public int R
        {
            get { return r; }
            set { r = value; }
        }

        public override string ToString()
        {
            return "Circle: x= " + point.X + " y= " + point.Y;
        }
    }
}
