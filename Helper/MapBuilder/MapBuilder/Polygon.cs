﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapBuilder
{
    class Polygon
    {

        private String id;

        public String Id
        {
            get { return id; }
            set { id = value; }
        }

        private List<Line> lines;

        public List<Line> Lines
        {
            get { return lines; }
            set { lines = value; }
        }

        public Polygon()
        {
            lines = new List<Line>();
        }

        public override string ToString()
        {
            Console.WriteLine("Polygon "+ id +" number of lines: " + lines.Count);
            foreach (Line line in lines){
                Console.WriteLine(line);
            }

            return "";
        }
    }
}
