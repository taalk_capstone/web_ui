﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapBuilder
{
    class Program
    {

        static void Main(string[] args)
        {
            //USE ONLY IF IT IS EXTREMELLY NECESSARY
            //BuildMapWithIds();
            //BuildNodes();
            //BuildHeatAreas();


            //Save to use anytime :)
            String file = @"C:\Users\ady\Desktop\Ady Capstone Project\Web_UI\data\map.txt";
            //CreateShapesFromFile(file);

            PathFinder pathFinder = new PathFinder();
            pathFinder.Run(); 

            Console.WriteLine("Press Enter for exit.");
            Console.ReadKey();
        }

        /// <summary>
        /// All the data is good to go to server, build the list of objects
        /// </summary>
        /// <param name="file">Where to read data from </param>
        public static void CreateShapesFromFile(String file)
        {
            // Read each line of the file into a string array. Each element
            // of the array is one line of the file.
            string[] fileLines = System.IO.File.ReadAllLines(file);

            List<Rectangle> rectangles = new List<Rectangle>();
            List<Polygon> polygons = new List<Polygon>();
            List<Circle> circles = new List<Circle>();
            List<Line> lines = new List<Line>();
            foreach (string shape in fileLines)
            {
                // Console.WriteLine(shape);
                if (shape.StartsWith("<rect"))
                {
                    Rectangle rect = new Rectangle();

                    String[] props = shape.Split(' ');

                    //after the shape itself comes the id
                    rect.Id = props[1];

                    int no;
                    if (int.TryParse(props[2], out no))
                    {
                        rect.Point.X = no;
                    }
                    if (int.TryParse(props[3], out no))
                    {
                        rect.Point.Y = no;
                    }
                    if (int.TryParse(props[4], out no))
                    {
                        rect.Width = no;
                    }
                    if (int.TryParse(props[5], out no))
                    {
                        rect.Height = no;
                    }

                    rectangles.Add(rect);
                }
                else if (shape.StartsWith("<polygon"))
                {
                    Polygon polygon = new Polygon();
                    List<Line> listLines = new List<Line>();
                    String[] points = shape.Split(' ');

                    // remove <tag, id, spaces and />
                    int size = points.Length - 4;

                    //id 
                    polygon.Id = points[1];

                    for (int i = 2; i < points.Length - 1; i++)
                    {
                        Line line = new Line();
                        String[] coordP1 = points[i].Split(',');

                        Boolean lastPoint = false;
                        //last point will join the first one
                        if (i == (points.Length - 3))
                        {
                            lastPoint = true;
                        }

                        if (coordP1.Length > 1)
                        {
                            int no;
                            Point p1 = new Point();
                            if (int.TryParse(coordP1[0], out no))
                            {
                                p1.X = no;

                                if (int.TryParse(coordP1[1], out no))
                                {
                                    p1.Y = no;
                                }
                                int index = i + 1;
                                if (lastPoint)
                                {
                                    //the first point in the list
                                    index = 2;
                                }
                                String[] coordP2 = points[index].Split(',');
                                int no2;
                                Point p2 = new Point();
                                if (int.TryParse(coordP2[0], out no2))
                                {
                                    p2.X = no2;
                                    if (int.TryParse(coordP2[1], out no2))
                                    {
                                        p2.Y = no2;
                                    }
                                }

                                line.Start = p1;
                                line.End = p2;
                                listLines.Add(line);
                            }
                        }
                    }
                    polygon.Lines = listLines;
                    polygons.Add(polygon);
                }
                else if (shape.StartsWith("<circle"))
                {
                    Circle circle = new Circle();

                    circles.Add(circle);
                }
                else if (shape.StartsWith("<line"))
                {
                    Line line = new Line();
                    String[] points = shape.Split(' ');

                    int no;
                    Point p1 = new Point();
                    if (int.TryParse(points[1], out no))
                    {
                        p1.X = no;

                        if (int.TryParse(points[2], out no))
                        {
                            p1.Y = no;
                        }

                        Point p2 = new Point();

                        if (int.TryParse(points[3], out no))
                        {
                            p2.X = no;
                        }
                        if (int.TryParse(points[4], out no))
                        {
                            p2.Y = no;
                        }

                        line.Start = p1;
                        line.End = p2;

                        lines.Add(line);
                    }

                }
            }

            foreach (Rectangle rect in rectangles)
            {
                Console.WriteLine(rect);
            }

            foreach (Polygon polyg in polygons)
            {
                Console.WriteLine(polyg);
            }

            foreach (Circle circle in circles)
            {
                Console.WriteLine(circle);
            }

            foreach (Line line in lines)
            {
                Console.WriteLine(line);
            }
        }

        /// <summary>
        /// Steps to build:
        /// 1) Draw the nodes
        /// 2) Export as SVG
        /// 3) Get the new group and paste on 'points.txt'
        /// 4) run this method
        /// 
        /// Create the nodes file based on the SVG raw data from the drawing
        /// </summary>
        public static void BuildNodes()
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\ady\Desktop\Ady Capstone Project\Web_UI\files\points.txt");

            System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\Users\ady\Desktop\Ady Capstone Project\Web_UI\data\nodes.txt");
            int circles = 0;
            foreach (string shape in lines)
            {
                Console.WriteLine(shape);
                if (shape.StartsWith("<circle"))
                {
                    Circle circle = new Circle();
                    circles++;
                    String circleLine = shape;
                    circleLine = circleLine.Replace("<circle ", "<circle id=\"" + circles + "\" ");

                    String newShape = formatShape(circleLine);
                    file.WriteLine(newShape);

                }
            }

            file.Close();
        }

        /// <summary>
        /// Getting the SVG raw data from the drawing, removing all the tags to leave the file only with the parameters to build the map objects
        /// </summary>
        public static void BuildMapWithIds()
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\ady\Desktop\Ady Capstone Project\Web_UI\map with id.txt");

            System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\Users\ady\Desktop\Ady Capstone Project\Web_UI\data\map.txt");
            foreach (string shape in lines)
            {
                String newShape = formatShape(shape);
                file.WriteLine(newShape);
            }

            Console.WriteLine("Writing file is done");
            file.Close();
        }

        /// <summary>
        /// Using the beacon information, build the heat area around it using radius:
        /// 4m
        /// 8m
        /// 12m
        /// </summary>
        public static void BuildHeatAreas()
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\ady\Desktop\Ady Capstone Project\Web_UI\data\beacons.txt");

            System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\Users\ady\Desktop\Ady Capstone Project\Web_UI\data\heat_area.txt");
            foreach (string shape in lines)
            {
                String[] parts = shape.Split(' ');
                // 4m 
                String newShape = "<circle " + parts[1] + " " + parts[2] + " " + parts[3] + " 22 />";
                file.WriteLine(newShape);
                // 8m 
                newShape = "<circle " + parts[1] + " " + parts[2] + " " + parts[3] + " 44 />";
                file.WriteLine(newShape);
                // 12m 
                // newShape = "<circle " + parts[1] + " " + parts[2] + " " + parts[3] + " 66 />";
                //file.WriteLine(newShape);

            }

            Console.WriteLine("Writing file is done");
            file.Close();
        }

        private static String formatShape(String shape)
        {
            String newShape = shape.Replace("\"", "").Replace("cx=", "").Replace("cy=", "")
                   .Replace("r=", "").Replace("id=", "").Replace("/>", " />").Replace("x=", "").Replace("y=", "")
                    .Replace("height=", "").Replace("width=", "").Replace("points=", "").Replace("/>", " />");

            return newShape;
        }
    }


}
