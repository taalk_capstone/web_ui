﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapBuilder
{
    class Line
    {
        public Line()
        {
            start = new Point();
            end = new Point();
        }

        private Point start;

        public Point Start
        {
            get { return start; }
            set { start = value; }
        }
        private Point end;

        public Point End
        {
            get { return end; }
            set { end = value; }
        }

        public override string ToString()
        {
            return "start: " + start + " end: " + end;
        }
    }
}
