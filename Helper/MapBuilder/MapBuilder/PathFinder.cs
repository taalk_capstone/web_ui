﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapBuilder
{
    public class Node
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public IEnumerable<Edge> Edges { get; set; }
    }

    public class Edge
    {
        public int OriginID { get; set; }
        public int DestinationID { get; set; }
        public Double Distance { get; set; }
    }

    public class PathFinder
    {
        private readonly Node[] _nodes;
        private List<int> _visitedNodes = new List<int>();
        private List<List<int>> _successfulPaths = new List<List<int>>();
        private List<int> _currentPath = new List<int>();
        public PathFinder(Node[] nodes)
        {
            _nodes = nodes;
        }

        public PathFinder()
        {

        }

        public int[] GetShortestPath(int sourceVertexId, int destinationVertexId)
        {

            Console.WriteLine("navigating from " + sourceVertexId + " to " + destinationVertexId);
            var v = (from n in _nodes
                     where n.ID == sourceVertexId
                     select n).FirstOrDefault();
            Search(v, destinationVertexId);
            int length = _nodes.Length;
            int[] path = new int[] { };
            foreach (var p in _successfulPaths)
            {
                if (p.Count < length)
                    path = p.ToArray();
            }
            return path;
        }

        private void Search(Node v, int destinationId)
        {
            if (v != null)
            {
                _visitedNodes.Add(v.ID);
                _currentPath.Add(v.ID);
                foreach (Edge e in v.Edges)
                {
                    if (e.DestinationID == destinationId)
                    {
                        // do not mark destination as visited
                        // this allows search to reach destination via different routes
                        // add current path to successful paths (fake last step!)
                        _currentPath.Add(e.DestinationID);
                        _successfulPaths.Add(_currentPath.ToList());
                        _currentPath.Remove(e.DestinationID); //reverse
                    }
                    else
                    {
                        if (!_visitedNodes.Contains(e.DestinationID))
                        {
                            var w = (from n in _nodes
                                     where n.ID == e.DestinationID
                                     select n).FirstOrDefault();
                            Search(w, destinationId);
                        }
                    }
                }
                _currentPath.Remove(v.ID); //reverse
            }
        }

        public void Example()
        {
            var
               vertices = new List<Node> {
                    new
                    Node {
                        ID
                            = 1,
                            Name = "1",
                            Edges = new [] {
                                new
                                Edge {
                                    OriginID = 1, DestinationID = 2
                                },
                                new
                                Edge {
                                    OriginID = 1, DestinationID = 3
                                }
                            }
                    },
                    new
                    Node {
                        ID
                            = 2,
                            Name = "2",
                            Edges = new [] {
                                new
                                Edge {
                                    OriginID = 2, DestinationID = 4
                                },
                                new
                                Edge {
                                    OriginID = 2, DestinationID = 5
                                }
                            }
                    },
                    new
                    Node {
                        ID
                            = 3,
                            Name = "3",
                            Edges = new [] {
                                new
                                Edge {
                                    OriginID = 3, DestinationID = 6
                                }
                            }
                    },
                    new
                    Node {
                        ID
                            = 4,
                            Name = "4",
                            Edges = new [] {
                                new
                                Edge {
                                    OriginID = 4, DestinationID = 1
                                },
                                new
                                Edge {
                                    OriginID = 4, DestinationID = 6
                                }
                            }
                    },
                    new
                    Node {
                        ID
                            = 5,
                            Name = "5",
                            Edges = new Edge[] {}
                    },
                    new
                    Node {
                        ID
                            = 6,
                            Name = "6",
                            Edges = new Edge[] {}
                    }
                };
            PathFinder
             dfs = new PathFinder(vertices.ToArray());
            var
             path = dfs.GetShortestPath(1, 6);

            foreach (int p in path)
            {
                Console.WriteLine(p);
            }
        }

        private static List<Node> BuildGraph()
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\ady\Desktop\Ady Capstone Project\Web_UI\data\FromToPoints.csv");

            Node node = new Node();
            List<Edge> edges = new List<Edge>();
            List<Node> nodes = new List<Node>();

            Array.Sort(lines);

            for (int i = 0; i < lines.Length; i++)
                {
                    Double weight = 0;
                    int from = 0;
                    int to = 0;

                    string line = lines[i];
                    //Console.WriteLine(line);
                    String[] points = line.Split(',');
                    
                    //next line
                    string nextLine = null;
                    if ((i+1) < lines.Length){
                        nextLine = lines[i+1];
                    }

                    if (int.TryParse(points[0], out from))
                    {

                        if (node.ID != from)
                        {
                            node = new Node();
                            //from
                            node.ID = from;
                            node.Name = from.ToString();
                            edges = new List<Edge>();
                        }

                        Edge edge = new Edge();
                        edge.OriginID = from;
                        if (int.TryParse(points[1], out to))
                        {
                            //to
                            edge.DestinationID = to;
                        }
                        if (double.TryParse(points[2], out weight))
                        {
                            //weight
                            edge.Distance = weight;
                        }
                        edges.Add(edge);

                        if (nextLine != null)  
                        {
                            String [] nextPoints = nextLine.Split(',');
                            if (nextPoints[0].Equals(points[0])) {
                                //same id, more than one edge
                                continue;
                            }
                        }

                        node.Edges = edges.ToArray();

                        nodes.Add(node);

                    }

                }

            Console.WriteLine("Building graph is done");

            return nodes;
        }

        public void Run()
        {

            List<Node> map = BuildGraph();
            if (map != null)
            {
                Console.WriteLine("Conestoga Map");
                PathFinder dfs = new PathFinder(map.ToArray());

                var path = dfs.GetShortestPath(35, 19);

                foreach (int p in path)
                {
                    Console.WriteLine(p);
                }
            }
        }

    }
           
}

