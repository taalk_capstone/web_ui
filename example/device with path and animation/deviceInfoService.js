//DeviceInfoService.js
'use strict';
app.factory('deviceInfoService',  function () {

	 var deviceInfoServiceFactory = {};

	 var deviceInfo = {
	 	deviceId: 0,
  		deviceName : '',
  		DevicePointsList : []
	 };

	 var devicePoints = []; 

	 var _setDeviceInfo = function(deviceInfoData){

	 	deviceInfo = deviceInfoData;
	 } 

	 var _getDeviceInfo = function(){

	 	return deviceInfo;
	 }

	 var _getDevicePoints = function(){
        var list = deviceInfo.DevicePointsList;
        
         //default devices
        if (list.length == 0){
            //device list
            var device1 = {'width': 32, 
                            'height': 32,
                            'initialX': 0,
                            'initialY': 0,
                            'pathId': 'svg_device1',
                            'image': 'img/butterfly_32x32.png',
                            'path': 'M10,256 L290,95 L230,266 L127,350 L136,225 L10,256 z',
                            'name': 'Ady iPhone',
                            'pathColor': '#9370db',
                            'battery': '100%'};
            var device2 = {'width': 32, 
                            'height': 32,
                            'initialX': 0,
                            'initialY': 0,
                            'pathId': 'svg_device2',
                            'image': 'img/icon.svg',
                            'path': 'M240,45 L1,216 L320,116 L387,295 L96,295 L1,216 z',
                            'name': 'Lalit iPhone',
                            'pathColor': '#ff9900',
                            'battery': '35%'};
            var device3 = {'width': 32, 
                            'height': 32,
                            'initialX': 0,
                            'initialY': 0,
                            'pathId': 'svg_device3',
                            'image': 'img/iphone_32x32.png',
                            'path': 'M320,116 L1,216 L240,45 L387,295 L96,295 L320,116 z',
                            'name': 'Kevin iPhone',
                            'pathColor': '#ff0000',
                            'battery': '35%'};
            var device4 = {'width': 32, 
                            'height': 32,
                            'initialX': 0,
                            'initialY': 0,
                            'pathId': 'svg_device4',
                            'image': 'img/iphone_32x32.png',
                            'path': 'M387,295 L100,216 L240,45 L230,450 L320,116 L96,295 L100,116 z',
                            'name': 'Tiago\'s phone',
                            'pathColor': '#0000ff',
                            'battery': '35%'};

            var device5 = {'width': 32, 
                            'height': 32,
                            'initialX': 0,
                            'initialY': 0,
                            'pathId': 'svg_device5',
                            'image': 'img/iphone_32x32.png',
                            'path': 'M127,350 L300,295 L100,216 L240,45 L230,450 L320,116 L96,295 L230,450 z',
                            'name': 'Yan\'s phone',
                            'pathColor': '#66cdaa',
                            'battery': '45%'};

            list = [device1, device2, device3, device4, device5];
        }
         
	 	return list;
	 }

	 deviceInfoServiceFactory.setDeviceInfo = _setDeviceInfo;
	 deviceInfoServiceFactory.getDeviceInfo = _getDeviceInfo;
	 deviceInfoServiceFactory.getDevicePoints = _getDevicePoints;

	 return deviceInfoServiceFactory;

});