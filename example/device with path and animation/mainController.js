var app = angular.module('app', []);

app.controller('mainController', [ '$scope', '$log', 'mapInfoService', 'webSocketService', 'beaconInfoService', 'deviceInfoService', 'heatAreaInfoService', 'nodeInfoService', '$rootScope', function($scope, $log, mapInfoService, webSocketService, beaconInfoService, deviceInfoService, heatAreaInfoService, nodeInfoService, $rootScope) {
    //group info
    $scope.groupName = "TAALK";
    $scope.groupMembers = "Tiago, Ady, Alex, Lalit and Kevin";
    
    
    //SVG content
    var width = window.innerWidth;
    var height = window.innerHeight;

    var randomX = d3.random.normal(width / 2, 80),
        randomY = d3.random.normal(height / 2, 80);

    var svg = d3.select("#mainpage").append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("viewBox", "-100 400 " + width + " " + height)
      .append("g")
        .call(d3.behavior.zoom().scaleExtent([1, 8]).on("zoom", zoom))
      ;

    /*svg.selectAll("circle")
        .data(data)
      .enter().append("circle")
        .attr("r", 2.5)
        .attr("transform", function(d) { return "translate(" + d + ")"; }); */
    
    // websocket connection
    webSocketService.connect();
    
    //get map 
    $rootScope.$on('UpdateMap', function(){
        updateMap();
    });
    
    $rootScope.$on('UpdateBeacon', function(){
        updateBeacons();
    });
    
    $rootScope.$on('UpdateHeatArea', function(){
        updateHeatAreas();
    });

    $rootScope.$on('UpdateDevice', function(){
        updateDevices();
    });
    
    $rootScope.$on('UpdateNode', function(){
        updateNodes();
    });

    function zoom() {
      svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }
    
    function updateWindow(){
        var x = window.innerWidth;
        var y = window.innerHeight;
        svg.attr("width", x).attr("height", y);
    }
    
    window.onresize = updateWindow;   
    
    
    function updateMap(){

         //define mapUnit
         var mapUnit = {
            Id : 0,
            unitName : "",
            lines : [],
            cricles : []
         }
         
           //lines
         mapUnits = mapInfoService.getMapUnits();
         var nSize = mapUnits.length;
        //all classrooms
        var group = svg.append("g")
                .attr("id", "classrooms");
         for (var i = 0; i < nSize; i++) {
            var mapUnit = mapUnits[i];
            
             //each classroom
             var classroom = group.append("g")
                .attr("id", mapUnit.unitName);
            //each classroom has a group of lines
            lines = mapUnit.lines;
            lines.forEach(function(line){
                
                classroom.append("line")
                     .attr("x1", line.start_X)
                     .attr("y1", line.start_Y)
                     .attr("x2", line.end_X)
                     .attr("y2", line.end_Y)
                     .attr("stroke-width", line.stroke_width)
                     .attr("stroke", line.color);
            });
         };
        
        //border
        svg.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", 775)
            .attr("height", 845)
            .attr("stroke", "none")
            .attr("fill",  "black")
            .attr("fill-opacity", 0.1);
        
        //apply all the changes, draw the map
        $scope.$apply();
        
        //request the beacons
         var request = {
                commandName : "601",
                commandParams: {id : "1" }
            }
         webSocketService.send(JSON.stringify(request));
        
        //request heat area
         var request = {
                commandName : "1101",
                commandParams: {id : "1" }
            }
         webSocketService.send(JSON.stringify(request));

    }
    
    function updateBeacons(){
        var beaconList = beaconInfoService.getBeaconInfo();
        var group = svg.append("g")
                .attr("id", "beacons");
        beaconList.forEach(function(beacon){
                
            group.append("circle")
                .attr("id", beacon.id)
                .attr("name", beacon.name)
                 .attr("cx", beacon.Pt_X)
                 .attr("cy", beacon.Pt_Y)
                .attr("r", beacon.height)
                 .attr("stroke-width", 0.4)
                 .attr("stroke", "red")
                .attr("fill", "red");
                
            });
        $scope.$apply();
        
        
    }
    
    function updateHeatAreas(){
        var heatAreaList = heatAreaInfoService.getHeatAreaInfo();
        var group = svg.append("g")
                .attr("id", "heatAreas");
        heatAreaList.forEach(function(heatArea){
                
            if (heatArea.radius < 45 ){
                var circle = group.append("circle")
                    .attr("id", heatArea.id)
                    .attr("name", heatArea.beaconId)
                     .attr("cx", heatArea.center_X)
                     .attr("cy", heatArea.center_Y)
                     .attr("r", heatArea.radius)
                     .attr("stroke-width", 0.4) ;
                if (heatArea.radius == 22){ //zone 1
                   circle.attr("stroke", "#78AB46")
                   .attr("stroke-opacity", 0.5)
                   .attr("fill-opacity", 0.5)
                    .attr("fill", "#78AB46");
                } else if (heatArea.radius == 44){ //zone 2
                     circle.attr("stroke", "#78AB46")
                     .attr("stroke-opacity", 0.4)
                     .attr("fill-opacity", 0.4)
                    .attr("fill", "#4A7023");
                }
            }

        });
        $scope.$apply();
        
        //request nodes
         var request = {
                commandName : "1001",
                commandParams: {id : "1" }
            }
         webSocketService.send(JSON.stringify(request));
    }
    
    
    $scope.showInfo = false;
    function updateDevices(){
        var deviceList = deviceInfoService.getDevicePoints();
    
         var group = svg.append("g")
                .attr("id", "devices");
        deviceList.forEach(function(device){
                
            group.append("path")
                .attr("id", device.pathId)
                 .attr("d", device.path)
                 .attr("fill", "none")
                 .attr("stroke-width", 2)
                 .attr("stroke", device.pathColor);            
        
         var link = group.append("a")
                .attr("href", "#");
                
        var title = link.append("title").text(device.name);
        
        var image = link.append("image")
            .attr("x", device.initialX)
             .attr("y", device.initialY)
             .attr("width", device.width)
             .attr("height", device.height)
             .attr("xlink:href", device.image)
            .on("click", function(){
                showDeviceInfo(device);
            });
            
        var animation = image.append("animateMotion")
            .attr("dur", "100s")
             .attr("repeatCount", "indefinite")
             .attr("rotate", "auto");
            
        animation.append("mpath")
            .attr("xlink:href", "#" + device.pathId);
            
        });
        
    }
    
    function updateNodes(){
        var nodeList = nodeInfoService.getNodeInfo();
        var group = svg.append("g")
                .attr("id", "nodes");
        nodeList.forEach(function(node){
                
            group.append("circle")
                .attr("id", node.id)
                .attr("name", node.name)
                 .attr("cx", node.center_X)
                 .attr("cy", node.center_Y)
                .attr("r", 2) //node.radius)
                 .attr("stroke-width", 2)
                 .attr("stroke", "#00A65B")
                .attr("fill", "#00A65B");
                
            });
        $scope.$apply();
    }
    
    function showDeviceInfo(device) {
        $scope.showInfo = true;
        $scope.device = device;
        
        openCloseInfo($scope.showInfo);
        
        $scope.$apply();
    };

}]);

app.directive('tooltip', function(){
    return {
        restrict: 'E',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});