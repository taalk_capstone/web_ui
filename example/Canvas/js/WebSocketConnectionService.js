//WebSocketConnectionService.js
'use strict'

app.factory('WebSocketConnectionService', function(){

	var WSFactory = {};
	var webSocket;
	var serverAddress = 'ws://localhost:54321'
    
    var _connect = function(){
        //var serverAddressInput = document.getElementById("serverAddress");
        webSocket = new WebSocket(serverAddress);

        webSocket.onopen = function(e) {
            changeState(true);
            log("Connection open...");
        };

        webSocket.onerror = function (e) {
            changeState(false);
            log("Connection error...");
        };

        webSocket.onmessage = function(e){
            if(typeof e.data === "string")
                log("Received : " + e.data);
            else
                log("Binary message received...");
        };
        webSocket.onclose = function(e){
            log("Connection Closed...");
            changeState(false);
        };
    };

    var disconnect = function(){
        log("Closing connection...");
        webSocket.close();
    };


    var log = function(message){
		var text = document.createTextNode(message);
        var div = document.createElement('div');
		div.appendChild(text);
        div.innerText = message;

        document.getElementById("messages").appendChild(div);
    };
    var changeState = function(isConnected){
        var container = document.getElementById("messageInputContainer");
        container.style.visibility=isConnected?"visible":"collapse";

        var connectButton = document.getElementById("connect");
        connectButton.disabled = isConnected;

        var disconnectButton = document.getElementById("disconnect");
        disconnectButton.disabled = !isConnected;
    };
    var _send = function() {
        if (webSocket.readyState != 1) {
            log("Cannot send data when the connection is closed...");
            return;
        }
        var messageInput = document.getElementById("message");
        var message = messageInput.value;
        log("Sending : " + message);
        webSocket.send(message);
    };

    WSFactory.connect = _connect;
    WSFactory.send = _send;

})


