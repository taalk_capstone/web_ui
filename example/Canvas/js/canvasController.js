window.requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

app.controller('canvasController', ['$scope', '$document', function($scope, $document){
    var canvas = $document.find('canvas')[0];
    var ctx    = canvas.getContext('2d');
    
    var devices = [];
    
    function Device(src, x, y, speed) {
        var img = new Image();
        img.src = src;
        this.image = img;
        this.x = x;
        this.y = y;
        this.speed = speed;
        
        this.counter = 0;
        
    }
    
    Device.prototype.update = function() {
        this.counter += this.speed;
        
        ctx.beginPath();
        ctx.drawImage(this.image, this.x + this.counter, this.y + this.counter);
        ctx.closePath();     
        
    };
    
    function drawDevices(){
        angular.forEach(getDevices(), function(value) {
            angular.forEach(value.path, function(coordinate) {
                
                /// set start point
    x1 = x2;
    y1 = y2;
                
                var device = new Device(value.image, coordinate.x, coordinate.y, 1);
                devices.push(device);
            });
            
        });
        
        draw();
    }
    
    drawDevices();
    

    function draw(){
    
        canvas.width  = window.innerWidth;
        canvas.height = window.innerHeight;
        
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        //background
        ctx.fillStyle = '#ffffff';
        ctx.fillRect(0, 0, window.innerWidth, window.innerHeight);
        
        drawMap(ctx);
        
        drawBeacons(ctx);
        
        //draw devices
        for (var i = 0; i < devices.length; i++) {
            var device = devices[i];
            device.update();
        }
        
        requestAnimationFrame(draw);
    }
    
    // Register an event listener to
    // call the draw() function each time 
    // the window is resized.
    window.addEventListener('resize', draw, false);
    window.addEventListener('orientationchange', draw, false);

    // Draw canvas border for the first time.
    //draw();

}]);

function getMap() {
    var floorPlan = {id: 'floor',
                    coordinates: [{x: 1, y: 216}, {x: 240, y: 45}, {x:480, y:216}, {x:387, y:495}, {x:96, y:495}, {x:1, y:216}]};
    return [floorPlan];
}

function drawMap(ctx){
    //floor plan
    ctx.scale(1,1);
    ctx.fillStyle = "#000";
    ctx.beginPath();
     angular.forEach(getMap(), function(value, key) {
         angular.forEach(value.coordinates, function(coordinate, key) {
            ctx.lineTo(coordinate.x,coordinate.y);
        });
    });
    ctx.closePath();
    ctx.stroke();
}

function getBeacons() {
    //beacon list
    var beacon1 = {
        'coordinates': {'x':99, 'y':145},
        'r': 3,
        'id': 'pointA'
    };
    
     var beacon2 = {
        'coordinates': {'x':315, 'y':100},
        'r': 3,
        'id': 'pointB'
    };
    
    var beacon3 = {
        'coordinates': {'x':435, 'y':350},
        'r': 3,
        'id': 'pointC'
    };
    
    return [beacon1, beacon2, beacon3];
}

function drawBeacons(ctx){
    //beacons
    ctx.scale(1,1);
    ctx.fillStyle = "#ff0000";

    angular.forEach(getBeacons(), function(beacon, key) {        
        ctx.beginPath();
        ctx.arc(beacon.coordinates.x, beacon.coordinates.y, 3, 0, 2 * Math.PI, false);
        ctx.closePath();
        ctx.fill();
    });
}

function getDevices(){
    var device1 = {
                    'initialX': 0,
                    'initialY': 0,
                    'pathId': 'svg_device1',
                    'image': 'img/butterfly_32x32.png',
                    'path': [{x: 10, y: 256}, {x: 290, y: 95}, //{x:230, y:266}, {x:127, y:350}, {x:136, y:225}, {x:10, y:256}],
                    'name': 'Ady iPhone',
                    'battery': '100%'};
    var device2 = {
                    'initialX': 0,
                    'initialY': 0,
                    'pathId': 'svg_device2',
                    'image': 'img/icon.svg',
                    'path': 'M240,45 L1,216 L320,116 L387,295 L96,295 L1,216 z',
                    'name': 'Lalit iPhone',
                    'battery': '35%'};
    var device3 = {
                    'initialX': 0,
                    'initialY': 0,
                    'pathId': 'svg_device3',
                    'image': 'img/iphone_32x32.png',
                    'path': 'M320,116 L1,216 L240,45 L387,295 L96,295 L320,116 z',
                    'name': 'Kevin iPhone',
                    'battery': '35%'};
    var device4 = {
                    'initialX': 0,
                    'initialY': 0,
                    'pathId': 'svg_device4',
                    'image': 'img/iphone_32x32.png',
                    'path': 'M387,295 L100,216 L240,45 L230,450 L320,116 L96,295 L100,116 z',
                    'name': 'Tiago\'s phone',
                    'battery': '35%'};
    
    var device5 = {
                    'initialX': 0,
                    'initialY': 0,
                    'pathId': 'svg_device5',
                    'image': 'img/iphone_32x32.png',
                    'path': 'M127,350 L300,295 L100,216 L240,45 L230,450 L320,116 L96,295 L230,450 z',
                    'name': 'Yan\'s phone',
                    'battery': '45%'};
    
    return [device1];//, device2, device3, device4, device5];
}