app.controller('mainController', function($scope) {
    //group info
    $scope.groupName = "TAALK";
    $scope.groupMembers = "Tiago, Ady, Alex, Lalit and Kevin";
    
    //device list
    $scope.device = {};
    
    $scope.showInfo = false;
    $scope.showDeviceInfo = function(id) {
        getDeviceInfo(id);
        $scope.showInfo = true;
    };
    
    $scope.showDeviceName = function(id) {
        angular.forEach($scope.deviceList, function(value, key) {
            if(value.pathId == id){
                $scope.deviceName = value.name;
            } 
        });
    };
    
    getDeviceInfo = function(id) {
        angular.forEach($scope.deviceList, function(value, key) {
            if(value.pathId == id){
                $scope.device = value;
            } 
        });
    };
    
    
});

app.directive('tooltip', function(){
    return {
        restrict: 'E',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});
